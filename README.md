#Fast Inventory
> Fast Inventory app in Electron

##Installation
Yarn requirement:
https://yarnpkg.com/en/docs/install

After yarn installation, we must install application dependencies, please run:
```
yarn install
```

##Development tasks
For app running:
```
yarn run start:dev
```

For tests running:
```
yarn run test
```

For tslint running:
```
yarn run lint
```

For app build running:
```
yarn run build
```

For packager:
```
yarn run package-all
```


##License

Copyright (c) 2017 Norbert Sajdok
