const chai = require('chai');
const chaiEnzyme = require('chai-enzyme');
const chaiJsx = require('chai-jsx');
const jsdom = require('jsdom').jsdom;

chai.use(chaiEnzyme());
chai.use(chaiJsx);

global.document = jsdom('<!doctype html><html><body></body></html>');
global.window = document.defaultView;
global.navigator = global.window.navigator;
