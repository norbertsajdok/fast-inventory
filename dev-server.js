/**
 * Setup and run the development server for Hot-Module-Replacement
 * https://webpack.github.io/docs/hot-module-replacement-with-webpack.html
 * @flow
 */

const express = require('express');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const spawn = require('child_process').spawn;
const config = require('./webpack.config.development');

const app = express();
const compiler = webpack(config);
const PORT = process.env.PORT || 3000;

const webpackMiddleware = webpackDevMiddleware(compiler, {
	publicPath: config.output.publicPath,
	stats: {
		colors: true
	}
});

app.use(webpackMiddleware);
app.use(webpackHotMiddleware(compiler));

const server = app.listen(PORT, 'localhost', function (serverError) {
	if (serverError) {
		return console.error(serverError);
	}

	spawn('yarn', ['run', 'start:electron'], { shell: true, env: process.env, stdio: 'inherit' })
		.on('close', function (code) { process.exit(code) })
		.on('error', function (spawnError) { console.error(spawnError) });

	console.log('Listening at http://localhost:' + PORT);
});

process.on('SIGTERM', function () {
	webpackMiddleware.close();
	server.close(function () {
		process.exit(0);
	});
});
