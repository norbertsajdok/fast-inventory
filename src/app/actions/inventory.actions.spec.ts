import { expect } from 'chai';
import {
	ACTIONS, createInventoryAction,
	createFetchInventoryAction, createFetchInventorySuccessAction, createFetchInventoryErrorAction,
	createAddProductAction, createAddProductSuccessAction, createAddProductErrorAction,
	createRemoveProductAction, createRemoveProductSuccessAction, createRemoveProductErrorAction,
	createEditProductAction, createEditProductSuccessAction, createEditProductErrorAction,
	createSearchProductAction, createSearchProductSuccessAction, createSearchProductErrorAction,
	createEditInventorySettingsAction, createEditInventorySettingsSuccessAction, createEditInventorySettingsErrorAction,
	createSelectProductToEditAction, createSelectInventorySettingsToEditAction, createUnSelectInventorySettingsToEditAction
} from './inventory.actions';
import { response } from '../fixtures/inventory';
import { Product, Inventory, InventorySettings } from '../../domain';

describe('Actions: inventory', () => {
	it('should create inventory action', () => {
		const expectedAction: Object = {
			type: ACTIONS.CREATE_INVENTORY
		};

		expect(createInventoryAction()).to.deep.equal(expectedAction);
	});

	it('should create fetch inventory action', () => {
		const expectedAction: Object = {
			type: ACTIONS.FETCH_INVENTORY
		};

		expect(createFetchInventoryAction()).to.deep.equal(expectedAction);
	});

	it('should create fetch inventory success action', () => {
		const expectedAction: Object = {
			type: ACTIONS.FETCH_INVENTORY_SUCCESS,
			inventory: new Inventory(response.inventory)
		};

		expect(createFetchInventorySuccessAction(response)).to.deep.equal(expectedAction);
	});

	it('should create fetch inventory error action', () => {
		const expectedAction: Object = {
			type: ACTIONS.FETCH_INVENTORY_ERROR,
			error: 'Fetch inventory error'
		};

		expect(createFetchInventoryErrorAction('Fetch inventory error')).to.deep.equal(expectedAction);
	});

	it('should create add product action', () => {
		const newProduct: any = {
			name: 'productName',
			unit: 'unit',
			quantity: 2,
			net_price: 2.50
		};

		const expectedAction: Object = {
			type: ACTIONS.ADD_PRODUCT,
			product: new Product({
				id: '0',
				name: newProduct.name,
				unit: newProduct.unit,
				quantity: newProduct.quantity,
				net_price: {
					amount: newProduct.net_price
				}
			})
		};

		expect(createAddProductAction(newProduct)).to.have.all.keys(expectedAction);
	});

	it('should create add product success action', () => {
		const expectedAction: Object = {
			type: ACTIONS.ADD_PRODUCT_SUCCESS,
			product: new Product(response.inventory.products[0])
		};

		expect(createAddProductSuccessAction(new Product(response.inventory.products[0]))).to.deep.equal(expectedAction);
	});

	it('should create add product error action', () => {
		const expectedAction: Object = {
			type: ACTIONS.ADD_PRODUCT_ERROR,
			error: 'Add product error'
		};

		expect(createAddProductErrorAction('Add product error')).to.deep.equal(expectedAction);
	});

	it('should create edit product action', () => {
		const productToEdit: Product = new Product(response.inventory.products[1]);
		const expectedAction: Object = {
			type: ACTIONS.EDIT_PRODUCT,
			product: productToEdit.update({
				name: 'productName'
			})
		};

		expect(createEditProductAction(new Product(response.inventory.products[1]), {
			name: 'productName'
		})).to.deep.equal(expectedAction);
	});

	it('should create edit product success action', () => {
		const expectedAction: Object = {
			type: ACTIONS.EDIT_PRODUCT_SUCCESS,
			product: new Product(response.inventory.products[1])
		};

		expect(createEditProductSuccessAction(new Product(response.inventory.products[1]))).to.deep.equal(expectedAction);
	});

	it('should create edit product error action', () => {
		const expectedAction: Object = {
			type: ACTIONS.EDIT_PRODUCT_ERROR,
			error: 'Edit product error'
		};

		expect(createEditProductErrorAction('Edit product error')).to.deep.equal(expectedAction);
	});

	it('should create remove product action', () => {
		const expectedAction: Object = {
			type: ACTIONS.REMOVE_PRODUCT,
			product: new Product(response.inventory.products[2])
		};

		expect(createRemoveProductAction(new Product(response.inventory.products[2]))).to.deep.equal(expectedAction);
	});

	it('should create remove product success action', () => {
		const expectedAction: Object = {
			type: ACTIONS.REMOVE_PRODUCT_SUCCESS,
			product: new Product(response.inventory.products[2])
		};

		expect(createRemoveProductSuccessAction(new Product(response.inventory.products[2]))).to.deep.equal(expectedAction);
	});

	it('should create remove product error action', () => {
		const expectedAction: Object = {
			type: ACTIONS.REMOVE_PRODUCT_ERROR,
			error: 'Remove product error'
		};

		expect(createRemoveProductErrorAction('Remove product error')).to.deep.equal(expectedAction);
	});

	it('should create search product action', () => {
		const expectedAction: Object = {
			type: ACTIONS.SEARCH_PRODUCT,
			query: 'example query'
		};

		expect(createSearchProductAction('example query')).to.deep.equal(expectedAction);
	});

	it('should create search product success action', () => {
		const expectedAction: Object = {
			type: ACTIONS.SEARCH_PRODUCT_SUCCESS,
			products: new Inventory(response.inventory).products
		};

		expect(createSearchProductSuccessAction(new Inventory(response.inventory).products)).to.deep.equal(expectedAction);
	});

	it('should create search product error action', () => {
		const expectedAction: Object = {
			type: ACTIONS.SEARCH_PRODUCT_ERROR,
			error: 'Search product error'
		};

		expect(createSearchProductErrorAction('Search product error')).to.deep.equal(expectedAction);
	});

	it('should create edit inventory settings action', () => {
		const expectedAction: Object = {
			type: ACTIONS.EDIT_INVENTORY_SETTINGS,
			settings: new InventorySettings({
				companyName: 'Sklep spożywczy',
				companyAddress: 'ul. 3 Maja 30, Katowice',
				committee: 'Jan Kowalski, Andrzej Nowak',
				others: 'Anna Kowalska',
			})
		};

		expect(createEditInventorySettingsAction({
			companyName: 'Sklep spożywczy',
			companyAddress: 'ul. 3 Maja 30, Katowice',
			committee: 'Jan Kowalski, Andrzej Nowak',
			others: 'Anna Kowalska',
		})).to.deep.equal(expectedAction);
	});

	it('should create edit inventory settings success action', () => {
		const inventory: Inventory = new Inventory(response.inventory);

		const expectedAction: Object = {
			type: ACTIONS.EDIT_INVENTORY_SETTINGS_SUCCESS,
			settings: inventory.settings
		};

		expect(createEditInventorySettingsSuccessAction({settings: inventory.settings})).to.deep.equal(expectedAction);
	});

	it('should create edit inventory settings error action', () => {
		const expectedAction: Object = {
			type: ACTIONS.EDIT_INVENTORY_SETTINGS_ERROR,
			error: 'Edit inventory settings error'
		};

		expect(createEditInventorySettingsErrorAction('Edit inventory settings error')).to.deep.equal(expectedAction);
	});

	it('should create select product to edit action', () => {
		const expectedAction: Object = {
			type: ACTIONS.SELECT_PRODUCT_TO_EDIT,
			product: new Product({
				id: '0',
				name: 'product name',
				unit: 'product unit',
				quantity: 3.75,
				net_price: {
					amount: 5.25
				}
			})
		};

		expect(createSelectProductToEditAction({
			id: '0',
			name: 'product name',
			unit: 'product unit',
			quantity: 3.75,
			net_price: {
				amount: 5.25
			}
		})).to.deep.equal(expectedAction);
	});

	it('should create select inventory settings to edit action', () => {
		const expectedAction: Object = {
			type: ACTIONS.SELECT_INVENTORY_SETTINGS_TO_EDIT
		};

		expect(createSelectInventorySettingsToEditAction()).to.deep.equal(expectedAction);
	});

	it('should create unselect inventory settings to edit action', () => {
		const expectedAction: Object = {
			type: ACTIONS.UNSELECT_INVENTORY_SETTINGS_TO_EDIT
		};

		expect(createUnSelectInventorySettingsToEditAction()).to.deep.equal(expectedAction);
	});
});
