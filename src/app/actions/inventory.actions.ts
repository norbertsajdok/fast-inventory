import { FIProduct, Product, Inventory, InventorySettings } from '../../domain';
import { InventoryResponse } from '../service/inventory.service';
import { UUID } from '../utils/uuid.class';

type ErrorAction = (error: string) => Object;

export const ACTIONS: any = {
	CREATE_INVENTORY: 'CREATE_INVENTORY',
	FETCH_INVENTORY: 'FETCH_INVENTORY',
	FETCH_INVENTORY_SUCCESS: 'FETCH_INVENTORY_SUCCESS',
	FETCH_INVENTORY_ERROR: 'FETCH_INVENTORY_ERROR',
	ADD_PRODUCT: 'ADD_PRODUCT',
	ADD_PRODUCT_SUCCESS: 'ADD_PRODUCT_SUCCESS',
	ADD_PRODUCT_ERROR: 'ADD_PRODUCT_ERROR',
	EDIT_PRODUCT: 'EDIT_PRODUCT',
	EDIT_PRODUCT_SUCCESS: 'EDIT_PRODUCT_SUCCESS',
	EDIT_PRODUCT_ERROR: 'EDIT_PRODUCT_ERROR',
	REMOVE_PRODUCT: 'REMOVE_PRODUCT',
	REMOVE_PRODUCT_SUCCESS: 'REMOVE_PRODUCT_SUCCESS',
	REMOVE_PRODUCT_ERROR: 'REMOVE_PRODUCT_ERROR',
	SEARCH_PRODUCT: 'SEARCH_PRODUCT',
	SEARCH_PRODUCT_SUCCESS: 'SEARCH_PRODUCT_SUCCESS',
	SEARCH_PRODUCT_ERROR: 'SEARCH_PRODUCT_ERROR',
	EDIT_INVENTORY_SETTINGS: 'EDIT_INVENTORY_SETTINGS',
	EDIT_INVENTORY_SETTINGS_SUCCESS: 'EDIT_INVENTORY_SETTINGS_SUCCESS',
	EDIT_INVENTORY_SETTINGS_ERROR: 'EDIT_INVENTORY_SETTINGS_ERROR',
	SELECT_PRODUCT_TO_EDIT: 'SELECT_PRODUCT_TO_EDIT',
	SELECT_INVENTORY_SETTINGS_TO_EDIT: 'SELECT_INVENTORY_SETTINGS_TO_EDIT',
	UNSELECT_INVENTORY_SETTINGS_TO_EDIT: 'UNSELECT_INVENTORY_SETTINGS_TO_EDIT'
};

export const createInventoryAction: () => any = (): any => {
	return {
		type: ACTIONS.CREATE_INVENTORY
	};
};

export const createFetchInventoryAction: () => any = (): any => {
	return {
		type: ACTIONS.FETCH_INVENTORY
	};
};

export const createFetchInventorySuccessAction: (response: InventoryResponse) => any = (response: InventoryResponse): any => {
	return {
		type: ACTIONS.FETCH_INVENTORY_SUCCESS,
		inventory: new Inventory(response.inventory)
	};
};

export const createFetchInventoryErrorAction: ErrorAction = (error: string): Object => {
	return {
		type: ACTIONS.FETCH_INVENTORY_ERROR,
		error: error
	};
};

export const createAddProductAction: (data: any) => any = (data: any): any => {
	const product: Product = new Product({
		id: UUID.generate(),
		name: data.name,
		unit: data.unit,
		quantity: +data.quantity,
		net_price: {
			amount: +data.net_price
		}
	});

	return {
		type: ACTIONS.ADD_PRODUCT,
		product: product
	};
};

export const createAddProductSuccessAction: (product: Product) => any = (product: Product): any => {
	return {
		type: ACTIONS.ADD_PRODUCT_SUCCESS,
		product: product
	};
};

export const createAddProductErrorAction: ErrorAction = (error: string): Object => {
	return {
		type: ACTIONS.ADD_PRODUCT_ERROR,
		error: error
	};
};

export const createEditProductAction: (product: Product, data: any) => any = (product: Product, data: any): any => {
	return {
		type: ACTIONS.EDIT_PRODUCT,
		product: product.update(data)
	};
};

export const createEditProductSuccessAction: (product: Product) => any = (product: Product): any => {
	return {
		type: ACTIONS.EDIT_PRODUCT_SUCCESS,
		product: product
	};
};

export const createEditProductErrorAction: ErrorAction = (error: string): Object => {
	return {
		type: ACTIONS.EDIT_PRODUCT_ERROR,
		error: error
	};
};

export const createRemoveProductAction: (product: Product) => any = (product: Product): any => {
	return {
		type: ACTIONS.REMOVE_PRODUCT,
		product: product
	};
};

export const createRemoveProductSuccessAction: (product: Product) => any = (product: Product): any => {
	return {
		type: ACTIONS.REMOVE_PRODUCT_SUCCESS,
		product: product
	};
};

export const createRemoveProductErrorAction: ErrorAction = (error: string): Object => {
	return {
		type: ACTIONS.REMOVE_PRODUCT_ERROR,
		error: error
	};
};

export const createSearchProductAction: (query: string) => any = (query: string): any => {
	return {
		type: ACTIONS.SEARCH_PRODUCT,
		query: query
	};
};

export const createSearchProductSuccessAction: (products: FIProduct[]) => any = (products: FIProduct[]): any => {
	return {
		type: ACTIONS.SEARCH_PRODUCT_SUCCESS,
		products: products.map((product: FIProduct) => new Product(product))
	};
};

export const createSearchProductErrorAction: ErrorAction = (error: string): Object => {
	return {
		type: ACTIONS.SEARCH_PRODUCT_ERROR,
		error: error
	};
};

export const createEditInventorySettingsAction: (data: any) => any = (data: any): any => {
	return {
		type: ACTIONS.EDIT_INVENTORY_SETTINGS,
		settings: new InventorySettings({
			companyName: data.companyName,
			companyAddress: data.companyAddress,
			committee: data.committee,
			others: data.others,
			startDate: data.startDate,
			endDate: data.endDate
		})
	};
};

export const createEditInventorySettingsSuccessAction: (data: any) => any = (data: any): any => {
	return {
		type: ACTIONS.EDIT_INVENTORY_SETTINGS_SUCCESS,
		settings: data.settings
	};
};

export const createEditInventorySettingsErrorAction: ErrorAction = (error: string): Object => {
	return {
		type: ACTIONS.EDIT_INVENTORY_SETTINGS_ERROR,
		error: error
	};
};

export const createSelectProductToEditAction: (product: FIProduct) => any = (product: FIProduct): any => {
	return {
		type: ACTIONS.SELECT_PRODUCT_TO_EDIT,
		product: new Product({
			id: product.id,
			name: product.name,
			unit: product.unit,
			quantity: product.quantity,
			net_price: {
				amount: product.net_price ? product.net_price.amount : 0
			}
		})
	};
};

export const createSelectInventorySettingsToEditAction: () => any = (): any => {
	return {
		type: ACTIONS.SELECT_INVENTORY_SETTINGS_TO_EDIT
	};
};

export const createUnSelectInventorySettingsToEditAction: () => any = (): any => {
	return {
		type: ACTIONS.UNSELECT_INVENTORY_SETTINGS_TO_EDIT
	};
};
