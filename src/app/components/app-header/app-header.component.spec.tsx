import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { expect } from 'chai';
import * as sinon from 'sinon';

import { AppHeaderComponent, AppHeaderProps } from './app-header.component';

describe('Component: app header', () => {
	let wrapper: ReactWrapper<{}, {}>;
	let props: AppHeaderProps;

	beforeEach(() => {
		props = {
			onCreateInventory: sinon.spy()
		};

		wrapper = mount(<AppHeaderComponent {...props} />);
	});

	it('should dispatch create inventory action', () => {
		const spy: any = props.onCreateInventory;
		wrapper.find('.create-inventory').simulate('click');
		expect(spy.calledOnce).to.equal(true);
	});
});
