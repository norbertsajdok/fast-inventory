import * as React from 'react';
import { Dispatch } from 'redux';
import { connect, MapStateToProps, MapDispatchToPropsFunction } from 'react-redux';
import { createInventoryAction } from '../../actions/inventory.actions';

import './app-header.component.styl';

export type AppHeaderProps = {
	onCreateInventory(): void
}

export class AppHeaderComponent extends React.Component<AppHeaderProps, void> {
	render(): JSX.Element {
		return (
			<div id="app-header">
				<div className="container">
					<h2>Fast Inventory</h2>
					<ul className="app-menu">
						<li className="create-inventory" onClick={this.handleClick.bind(this)}>
							<i className="glyphicon glyphicon-folder-open"></i>
							<span>nowy spis</span>
						</li>
					</ul>
				</div>
			</div>
		);
	}

	private handleClick(): void {
		this.props.onCreateInventory();
	}
}

const mapStateToProps: MapStateToProps<{}, {}> = (): Object => {
	return {};
};

const mapDispatchToProps: MapDispatchToPropsFunction<{}, {}> = (dispatch: Dispatch<any>): Object => {
	return {
		onCreateInventory(): void {
			dispatch(createInventoryAction());
		}
	}
};

export const ConnectAppHeaderComponent: React.ComponentClass<{}> = connect(mapStateToProps, mapDispatchToProps)(AppHeaderComponent);
