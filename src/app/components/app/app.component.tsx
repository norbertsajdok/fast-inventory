import * as React from 'react';
import { ConnectAppHeaderComponent } from '../app-header/app-header.component';

export class AppComponent extends React.Component<void, void> {
	render(): JSX.Element {
		return (
			<div className="app-container">
				<ConnectAppHeaderComponent/>
				<div className="container">
					{ this.props.children }
				</div>
			</div>
		);
	}
}
