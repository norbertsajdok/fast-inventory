import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { expect } from 'chai';
import * as sinon from 'sinon';

import { AutocompleteComponent, AutocompleteProps } from './autocomplete.component';
import { Inventory } from '../../../domain';
import { response } from '../../fixtures/inventory';

describe('Component: autocomplete', () => {
	let wrapper: ReactWrapper<{}, {}>;
	let props: AutocompleteProps;

	beforeEach(() => {
		props = {
			items: (new Inventory(response.inventory)).products,
			searchInput: document.createElement('input'),
			onSelect: sinon.spy()
		};

		wrapper = mount(<AutocompleteComponent {...props} />);
	});

	it('should have a search input', () => {
		expect(wrapper.find('.autocomplete-item')).to.have.length(6);
	});

	it('should have a name in autocomplete item', () => {
		expect(wrapper.find('.name').first().text()).to.equal('Sprite');
	});

	it('should have a price in autocomplete item', () => {
		expect(wrapper.find('.price').first().text()).to.equal('3.54 PLN');
	});

	it('should dispatch Select action on click on choosen item', () => {
		const spy: any = props.onSelect;
		wrapper.find('.autocomplete-item').first().simulate('click');
		expect(spy.calledOnce).to.equal(true);
	});

	it('should dispatch mouse events', () => {
		const item: ReactWrapper<{}, {}> = wrapper.find('.autocomplete-item').last();

		item.simulate('mouseover');
		expect(item.hasClass('highlight')).to.equal(true);
		item.simulate('mouseout');
		expect(item.hasClass('highlight')).to.equal(false);
	});
});
