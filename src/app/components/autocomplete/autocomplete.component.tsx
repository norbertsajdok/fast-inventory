import * as React from 'react';
import { FIProduct, Product } from '../../../domain';

import './autocomplete.component.styl';

export type AutocompleteProps = {
	items: Product[],
	searchInput: HTMLInputElement,
	onSelect(product: FIProduct): void
}

const HOLDER_ID: string = 'autocomplete-holder';
const ITEM_CLASS: string = 'autocomplete-item';
const HIGHLIGHT_CLASS: string = 'highlight';

export class AutocompleteComponent extends React.Component<AutocompleteProps, void> {
	private holder: HTMLDivElement;

	constructor() {
		super();

		this.hideOnBody = this.hideOnBody.bind(this);
	}

	componentDidMount(): void {
		document.body.addEventListener('click', this.hideOnBody, false);
	}

	componentDidUpdate(): void {
		this.show();
	}

	componentWillUnmount(): void {
		document.body.removeEventListener('click', this.hideOnBody, false);
	}

	render(): JSX.Element {
		return (
			<div id={HOLDER_ID} ref={(div: HTMLDivElement) => this.holder = div}>
				<ul>
					{this.props.items.map((product: Product, index: number): JSX.Element => {
						return <li key={index}
									className={ITEM_CLASS}
									onClick={this.handleClick.bind(this, product)}
									onMouseOver={this.handleMouseOver.bind(this)}
									onMouseOut={this.handleMouseOut.bind(this)}>
							<div className="name">{product.name}</div>
							<div className="price">{product.net_price.toString()}</div>
						</li>
					})}
				</ul>
			</div>
		);
	}

	private handleClick(product: Product): void {
		this.props.searchInput.value = '';

		this.props.onSelect(product);

		this.hide();
	}

	private handleMouseOver(event: React.FormEvent<HTMLLIElement>): void {
		const target: HTMLLIElement = event.currentTarget as HTMLLIElement;

		target.classList.add(HIGHLIGHT_CLASS);
	}

	private handleMouseOut(event: React.FormEvent<HTMLLIElement>): void {
		const target: HTMLLIElement = event.currentTarget as HTMLLIElement;

		target.classList.remove(HIGHLIGHT_CLASS);
	}

	private show(): void {
		this.holder.classList.remove('hidden');
	}

	private hide(): void {
		this.holder.classList.add('hidden');
	}

	private hideOnBody(event: Event): void {
		const target: HTMLElement = event.target as HTMLElement;
		if (target.id !== HOLDER_ID) {
			this.hide();
		}
	}
}
