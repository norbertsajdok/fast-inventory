import * as React from 'react';
import { Dispatch } from 'redux';
import { connect, MapStateToProps, MapDispatchToPropsFunction } from 'react-redux';
import { State } from '../../state';
import { Product } from '../../../domain';
import { createEditProductAction } from '../../actions/inventory.actions';
import { PrettyForm, FieldText, FieldSelect, SelectOption} from '../pretty-forms';

import './edit-product-form.component.styl';

export type EditProductFormProps = {
	product: Product,
	onEditProduct(product: Product, data: Object): void;
}

type EditProductFormState = {
	name: string;
	unit: string;
	quantity: string;
	net_price: string;
}

const unitSelectOptions: SelectOption[] = [{
	label: 'Sztuki',
	value: 'szt.'
}, {
	label: 'Kilogramy',
	value: 'kg.'
}];

export class EditProductFormComponent extends React.Component<EditProductFormProps, EditProductFormState> {
	state: EditProductFormState = {
		name: '',
		unit: 'szt.',
		quantity: '',
		net_price: ''
	};

	private layer: HTMLElement;

	componentDidMount(): void {
		this.bindEvents();
	}

	componentWillReceiveProps(nextProps: EditProductFormProps): void {
		const isProductChange: boolean = nextProps.product !== this.props.product;

		if (isProductChange && nextProps.product) {
			this.setState(Object.assign({}, this.state, {
				name: nextProps.product.name,
				unit: nextProps.product.unit,
				quantity: nextProps.product.quantity.toString(),
				net_price: nextProps.product.net_price.toString()
			}));

			this.show();
		}
	}

	render(): JSX.Element {
		return (
			<div className="layer hidden" ref={(div: HTMLElement) => this.layer = div}>
				<i className="layer-close glyphicon glyphicon-remove" onClick={this.hide.bind(this)}></i>
				<div id="edit-product-form" className="layer-content">
					<div className="form-header">
						<h2>Edytuj produkt</h2>
					</div>
					<PrettyForm onSubmit={this.handleSubmit.bind(this)}>
						<FieldText name="name" value={this.state.name} label="Nazwa produktu" required
									onChange={this.handleChange.bind(this)} errorMessage="Nazwa produktu jest wymagana"/>
						<FieldSelect name="unit" value={this.state.unit} label="Jednostka miary" required options={unitSelectOptions}
									onChange={this.handleChange.bind(this)} errorMessage="Jednostka miary jest wymagana"/>
						<FieldText name="quantity" value={this.state.quantity} label="Ilość"
									onChange={this.handleChange.bind(this)} />
						<FieldText name="net_price" value={this.state.net_price} label="Cena netto"
									onChange={this.handleChange.bind(this)} />
						<button type="submit" className="btn btn-default">
							<span className="glyphicon glyphicon-ok" aria-hidden="true"></span> edytuj
						</button>
					</PrettyForm>
				</div>
			</div>
		);
	}

	private handleChange(name: string, value: string): void {
		this.setState(Object.assign({}, this.state, {[name]: value}));
	}

	private handleSubmit(): void {
		this.props.onEditProduct(this.props.product, this.state);
		this.hide();
	}

	private show(): void {
		this.layer.classList.remove('hidden');
	}

	private hide(): void {
		this.layer.classList.add('hidden');
	}

	private bindEvents(): void {
		this.layer.addEventListener('click', (event: Event): void => {
			if (event.target === this.layer) {
				this.hide();
			}
		}, true);
	}
}

const mapStateToProps: MapStateToProps<{}, {}> = (state: State): Object => {
	return {
		product: state.search.selectedProduct
	}
};

const mapDispatchToProps: MapDispatchToPropsFunction<{}, {}> = (dispatch: Dispatch<any>): Object => {
	return {
		onEditProduct(product: Product, data: Object): void {
			dispatch(createEditProductAction(product, data));
		}
	}
};

export const ConnectedEditProductFormComponent: React.ComponentClass<{}> = connect(mapStateToProps, mapDispatchToProps)(EditProductFormComponent);
