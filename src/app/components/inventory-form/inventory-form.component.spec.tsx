import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { expect } from 'chai';
import * as sinon from 'sinon';

import { InventoryFormComponent, InventoryFormProps } from './inventory-form.component';
import { Inventory } from '../../../domain';
import { response } from '../../fixtures/inventory';

const fields: any = {
	companyName: 'input[name="companyName"]',
	companyAddress: 'input[name="companyAddress"]',
	committee: 'input[name="committee"]',
	others: 'input[name="others"]',
	startDate: 'input[name="startDate"]',
	endDate: 'input[name="endDate"]',
	submit: 'button[type="submit"]'
};

describe('Component: inventory form', () => {
	let wrapper: ReactWrapper<{}, {}>;
	let props: InventoryFormProps;

	beforeEach(() => {
		const inventory: Inventory = new Inventory(response.inventory);

		props = {
			inventorySettingsEditing: true,
			settings: inventory.settings,
			onSubmit: sinon.spy(),
			onHide: sinon.spy()
		};

		wrapper = mount(<InventoryFormComponent {...props} />);
	});

	it('should have initial state', () => {
		expect(wrapper.state()).to.deep.equal({
			committee: '',
			companyAddress: '',
			companyName: '',
			others: '',
			startDate: '',
			endDate: ''
		});
	});

	it('should have a company name input', () => {
		expect(wrapper.find(fields.companyName)).to.have.length(1);
	});

	it('should have a company address select', () => {
		expect(wrapper.find(fields.companyAddress)).to.have.length(1);
	});

	it('should have a inventory committee input', () => {
		expect(wrapper.find(fields.committee)).to.have.length(1);
	});

	it('should have a inventory others people input', () => {
		expect(wrapper.find(fields.others)).to.have.length(1);
	});

	it('should have a inventory start date', () => {
		expect(wrapper.find(fields.startDate)).to.have.length(1);
	});

	it('should have a inventory end date', () => {
		expect(wrapper.find(fields.endDate)).to.have.length(1);
	});

	it('should have a submit button', () => {
		expect(wrapper.find(fields.submit)).to.have.length(1);
	});

	describe('on change', () => {
		it('should set state for company name', () => {
			wrapper.find(fields.companyName).simulate('change', {target: {name: 'companyName', value: 'Company name'}});

			expect(wrapper.state('companyName')).to.deep.equal('Company name')
		});

		it('should set state for company address', () => {
			wrapper.find(fields.companyAddress).simulate('change', {target: {name: 'companyAddress', value: 'Company address'}});

			expect(wrapper.state('companyAddress')).to.deep.equal('Company address')
		});

		it('should set state for committee', () => {
			wrapper.find(fields.committee).simulate('change', {target: {name: 'committee', value: 'John Q'}});

			expect(wrapper.state('committee')).to.deep.equal('John Q')
		});

		it('should set state for others', () => {
			wrapper.find(fields.others).simulate('change', {target: {name: 'others', value: 'Pete Q'}});

			expect(wrapper.state('others')).to.deep.equal('Pete Q')
		});

		it('should set state for start date', () => {
			wrapper.find(fields.startDate).simulate('change', {target: {name: 'startDate', value: '2016-12-27T12:25'}});

			expect(wrapper.state('startDate')).to.deep.equal('2016-12-27T12:25')
		});

		it('should set state for end date', () => {
			wrapper.find(fields.endDate).simulate('change', {target: {name: 'endDate', value: '2016-12-27T14:25'}});

			expect(wrapper.state('endDate')).to.deep.equal('2016-12-27T14:25')
		});
	});

	describe('on submit', () => {
		it('should dispatch Submit action', () => {
			const spy: any = props.onSubmit;
			wrapper.find('form').simulate('submit');
			expect(spy.calledOnce).to.equal(true);
		});

		it('should dispatch Hide action', () => {
			const spy: any = props.onHide;
			wrapper.find('form').simulate('submit');
			expect(spy.calledOnce).to.equal(true);
		});
	});

});
