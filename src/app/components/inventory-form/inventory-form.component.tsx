import * as React from 'react';
import { Dispatch } from 'redux';
import { connect, MapStateToProps, MapDispatchToPropsFunction } from 'react-redux';
import { State } from '../../state';
import { createEditInventorySettingsAction, createUnSelectInventorySettingsToEditAction } from '../../actions/inventory.actions';
import { InventorySettings } from '../../../domain';
import { PrettyForm, FieldText } from '../pretty-forms';

import './inventory-form.component.styl';

export type InventoryFormProps = {
	inventorySettingsEditing: boolean,
	settings: InventorySettings,
	onSubmit(data: Object): void,
	onHide(): void
}

type InventoryFormState = {
	companyName: string,
	companyAddress: string,
	committee: string,
	others: string,
	startDate: string,
	endDate: string
}

export class InventoryFormComponent extends React.Component<InventoryFormProps, InventoryFormState> {
	state: InventoryFormState = {
		companyName: '',
		companyAddress: '',
		committee: '',
		others: '',
		startDate: '',
		endDate: ''
	};

	private layer: HTMLElement;

	componentDidMount(): void {
		this.bindEvents();
	}

	componentWillReceiveProps(nextProps: InventoryFormProps): void {
		if (nextProps.settings) {
			this.setState(Object.assign({}, this.state, {
				companyName: nextProps.settings.companyName || '',
				companyAddress: nextProps.settings.companyAddress || '',
				committee: nextProps.settings.committee || '',
				others: nextProps.settings.others || '',
				startDate: nextProps.settings.startDate || '',
				endDate: nextProps.settings.endDate || ''
			}));
		}

		if (nextProps.inventorySettingsEditing) {
			this.show();
		}
	}

	render(): JSX.Element {
		return (
			<div className="layer hidden" ref={(div: HTMLElement) => this.layer = div}>
				<i className="layer-close glyphicon glyphicon-remove" onClick={this.hide.bind(this)}></i>
				<div id="inventory-form" className="layer-content">
					<div className="form-header">
						<h2>Aktualny spis</h2>
					</div>
					<PrettyForm onSubmit={this.handleSubmit.bind(this)}>
						<FieldText name="companyName" value={this.state.companyName} label="Nazwa jednostki inwentaryzowanej"
									required onChange={this.handleChange.bind(this)} errorMessage="Nazwa jednostki jest wymagana"/>
						<FieldText name="companyAddress" value={this.state.companyAddress} label="Adres jednostki inwentaryzowanej"
									required onChange={this.handleChange.bind(this)} errorMessage="Adres jednostki jest wymagany"/>
						<FieldText name="committee" value={this.state.committee} label="Skład komisji"
									required onChange={this.handleChange.bind(this)} errorMessage="Skład komisji jest wymagany"/>
						<FieldText name="others" value={this.state.others} label="Inne osoby obecne przy spisie"
									onChange={this.handleChange.bind(this)} />
						<FieldText name="startDate" type="datetime-local" value={this.state.startDate} label="Data rozpoczęcia spisu"
									required onChange={this.handleChange.bind(this)} errorMessage="Data rozpoczęcia spisu jest wymagana" />
						<FieldText name="endDate" type="datetime-local" value={this.state.endDate} label="Data zakończenia spisu"
									required onChange={this.handleChange.bind(this)} errorMessage="Data zakończenia spisu jest wymagana" />
						<button type="submit" className="btn btn-default">
							<span className="glyphicon glyphicon-ok" aria-hidden="true"></span> zapisz ustawienia
						</button>
					</PrettyForm>
				</div>
			</div>
		);
	}

	private handleChange(name: string, value: string): void {
		this.setState(Object.assign({}, this.state, {[name]: value}));
	}

	private handleSubmit(): void {
		this.props.onSubmit(this.state);
		this.hide();
	}

	private show(): void {
		this.layer.classList.remove('hidden');
	}

	private hide(): void {
		this.layer.classList.add('hidden');
		this.props.onHide();
	}

	private bindEvents(): void {
		this.layer.addEventListener('click', (event: Event): void => {
			if (event.target === this.layer) {
				this.hide();
			}
		}, true);
	}
}

const mapStateToProps: MapStateToProps<{}, {}> = (state: State): Object => {
	return {
		inventorySettingsEditing: state.inventorySettingsEditing,
		settings: state.inventory.settings
	}
};

const mapDispatchToProps: MapDispatchToPropsFunction<{}, {}> = (dispatch: Dispatch<any>): Object => {
	return {
		onSubmit(data: Object): void {
			dispatch(createEditInventorySettingsAction(data))
		},
		onHide(): void {
			dispatch(createUnSelectInventorySettingsToEditAction());
		},
	}
};

export const ConnectedInventoryFormComponent: React.ComponentClass<{}> = connect(mapStateToProps, mapDispatchToProps)(InventoryFormComponent);
