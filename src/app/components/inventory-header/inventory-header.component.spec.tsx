import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { expect } from 'chai';

import { InventoryHeaderComponent, InventoryHeaderProps } from './inventory-header.component';
import { Inventory, FIProduct } from '../../../domain';
import { response } from '../../fixtures/inventory';

describe('Component: inventory header', () => {
	let wrapper: ReactWrapper<{}, {}>;
	let props: InventoryHeaderProps;

	beforeEach(() => {
		const inventory: Inventory = new Inventory(response.inventory);

		props = {
			products: inventory.products,
			searchItems: inventory.products,
			settings: inventory.settings,
			onSelectInventorySettingsToEdit: (): void => void {},
			onSearchProduct: (query: string): void => void {},
			onSelectProductToEdit: (product: FIProduct): void => {}
		};

		wrapper = mount(<InventoryHeaderComponent {...props} />);
	});

	it('should have inventory settings', () => {
		expect(wrapper.find('#inventory-settings').length).to.equal(1);
	});

	it('should have products count', () => {
		expect(wrapper.find('.products-count').text()).to.equal('Liczba pozycji w spisie: 6');
	});

	it('should have products value', () => {
		expect(wrapper.find('.products-value').text()).to.equal('Wartość: 290.49 PLN');
	});

});
