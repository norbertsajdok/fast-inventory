import * as React from 'react';
import { Dispatch } from 'redux';
import { connect, MapStateToProps, MapDispatchToPropsFunction } from 'react-redux';
import { Link } from 'react-router';
import { State } from '../../state';
import { FIProduct, Product, Price, InventorySettings } from '../../../domain';
import { createSelectInventorySettingsToEditAction, createSearchProductAction, createSelectProductToEditAction } from '../../actions/inventory.actions';
import { InventorySettingsComponent } from '../inventory-settings/inventory-settings.component';
import { InventorySearchComponent } from '../inventory-search/inventory-search.component';

import './inventory-header.component.styl';

export type InventoryHeaderProps = {
	products: Product[],
	searchItems: Product[],
	settings: InventorySettings,
	onSelectInventorySettingsToEdit(): void,
	onSearchProduct(query: string): void,
	onSelectProductToEdit(product: FIProduct): void
}

export class InventoryHeaderComponent extends React.Component<InventoryHeaderProps, void> {

	render(): JSX.Element {
		return (
			<div id="inventory-header">
				<div className="row head-belt">
					<div className="col-md-12">
						<h3>Spis z natury</h3>
						{this.props.products.length && this.props.settings ? (
							<Link to="/print" className="print-trigger">
								<span className="glyphicon glyphicon-print" aria-hidden="true"></span>
								<span className="text">Wydrukuj spis</span>
							</Link>
						) : null}
					</div>
				</div>
				<div className="row main-belt">
					<div className="col-md-5">
						<InventorySettingsComponent
							settings={this.props.settings}
							onSelectInventorySettingsToEdit={this.props.onSelectInventorySettingsToEdit}
						/>
					</div>
					<div className="col-md-3">
						<ul>
							<li className="products-count">Liczba pozycji w spisie: {this.props.products.length}</li>
							<li className="products-value">Wartość: {this.getProductsValue().toString()}</li>
						</ul>
					</div>
					<div className="col-md-4">
						<InventorySearchComponent searchItems={this.props.searchItems} onSearchProduct={this.props.onSearchProduct} onSelectProductToEdit={this.props.onSelectProductToEdit}/>
					</div>
				</div>
			</div>
		);
	}

	private getProductsValue(): Price {
		let productsValue: number = 0;

		this.props.products.forEach((product: Product) => {
			productsValue += product.net_value.amount;
		});

		return new Price({amount: productsValue});
	}
}

const mapStateToProps: MapStateToProps<{}, {}> = (state: State): Object => {
	return {
		products: state.inventory.products,
		searchItems: state.search.products,
		settings: state.inventory.settings
	}
};

const mapDispatchToProps: MapDispatchToPropsFunction<{}, {}> = (dispatch: Dispatch<any>): Object => {
	return {
		onSelectInventorySettingsToEdit(): void {
			dispatch(createSelectInventorySettingsToEditAction());
		},
		onSearchProduct(query: string): void {
			dispatch(createSearchProductAction(query));
		},
		onSelectProductToEdit(product: FIProduct): void {
			dispatch(createSelectProductToEditAction(product));
		}
	}
};

export const ConnectedInventoryHeaderComponent: React.ComponentClass<{}> = connect(mapStateToProps, mapDispatchToProps)(InventoryHeaderComponent);
