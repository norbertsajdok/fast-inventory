import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { expect } from 'chai';
import * as sinon from 'sinon';

import { InventoryNotificationComponent, InventoryNotificationProps } from './inventory-notification.component';

describe('Component: inventory notification', () => {
	let wrapper: ReactWrapper<{}, {}>;
	let props: InventoryNotificationProps;
	let clock: sinon.SinonFakeTimers;

	beforeEach(() => {
		clock = sinon.useFakeTimers();

		props = {
			notification: {
				type: 'success',
				text: 'Notification text'
			}
		};

		wrapper = mount(<InventoryNotificationComponent {...props} />);
	});

	afterEach(() => {
		clock.restore();
	});

	it('should have notification type as className of wrapper', () => {
		expect(wrapper).to.have.className(props.notification.type);
	});

	it('should have notification text', () => {
		expect(wrapper.find('.text')).to.have.text(props.notification.text);
	});

	describe('after 2500ms', () => {
		beforeEach(() => {
			wrapper.instance().forceUpdate();
			clock.tick(2510);
		});

		it('should set default className of wrapper', () => {
			expect(wrapper).to.have.className('default');
		});
	});
});
