import * as React from 'react';
import { connect, MapStateToProps } from 'react-redux';
import { State, Notification } from '../../state';

import './inventory-notification.component.styl';

export type InventoryNotificationProps = {
	notification: Notification;
}

const TIME_TO_HIDE: number = 2500;

export class InventoryNotificationComponent extends React.Component<InventoryNotificationProps, void> {
	private timeoutToHide: any;
	private notificationBox: HTMLDivElement;

	componentDidUpdate(): void {
		clearTimeout(this.timeoutToHide);

		this.timeoutToHide = setTimeout((): void => {
			this.notificationBox.className = '';
			this.notificationBox.classList.add('default');
		}, TIME_TO_HIDE);
	}

	componentWillUnmount(): void {
		clearTimeout(this.timeoutToHide);
	}

	render(): JSX.Element {
		return (
			<div id="inventory-notification" className={this.props.notification.type} ref={(div: HTMLDivElement) => this.notificationBox = div}>
				<span className="glyphicon glyphicon-ok" aria-hidden="true"></span>
				<span className="text">{this.props.notification.text}</span>
			</div>
		);
	}
}

const mapStateToProps: MapStateToProps<{}, {}> = (state: State): Object => {
	return {
		notification: state.notification
	}
};

export const ConnectedInventoryNotificationComponent: React.ComponentClass<{}> = connect(mapStateToProps)(InventoryNotificationComponent);
