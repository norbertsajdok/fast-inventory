import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { expect } from 'chai';
import * as sinon from 'sinon';

import { InventorySearchComponent, InventorySearchProps } from './inventory-search.component';
import { FIProduct } from '../../../domain';

describe('Component: inventory search', () => {
	let wrapper: ReactWrapper<{}, {}>;
	let props: InventorySearchProps;
	let clock: sinon.SinonFakeTimers;

	beforeEach(() => {
		clock = sinon.useFakeTimers();

		props = {
			searchItems: [],
			onSearchProduct: sinon.spy(),
			onSelectProductToEdit: (product: FIProduct) => {}
		};

		wrapper = mount(<InventorySearchComponent {...props} />);
	});

	it('should have a search input', () => {
		expect(wrapper.find('#search-products')).to.have.length(1);
	});

	it('should dispatch SearchProduct action on change search input with value longer than 2 chars', () => {
		const spy: any = props.onSearchProduct;
		wrapper.find('#search-products').simulate('change', {target: {value: 'example query'}});
		clock.tick(310);
		expect(spy.calledOnce).to.equal(true);
	});

	it('should not dispatch SearchProduct action on change search input with value longer than 2 chars', () => {
		const spy: any = props.onSearchProduct;
		wrapper.find('#search-products').simulate('change', {target: {value: 'ex'}});
		clock.tick(310);
		expect(spy.calledOnce).to.equal(false);
	});
});
