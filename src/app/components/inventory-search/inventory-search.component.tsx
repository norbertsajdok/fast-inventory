import * as React from 'react';
import { FIProduct, Product } from '../../../domain';
import { AutocompleteComponent } from '../autocomplete/autocomplete.component';

import './inventory-search.component.styl';

export type InventorySearchProps = {
	searchItems: Product[],
	onSearchProduct(value: string): void,
	onSelectProductToEdit(product: FIProduct): void
}

const MIN_QUERY_LENGTH: number = 3;

export class InventorySearchComponent extends React.Component<InventorySearchProps, void> {
	private onChangeTimeout: any;
	private input: HTMLInputElement;

	render(): JSX.Element {
		return (
			<div id="inventory-search">
				<div className="form-group">
					<input type="text" name="searchProducts" className="form-control" id="search-products"
							placeholder="Szukaj" onChange={this.handleChange.bind(this)} ref={(input: HTMLInputElement) => this.input = input}/>
				</div>
				<AutocompleteComponent searchInput={this.input} items={this.props.searchItems}  onSelect={this.props.onSelectProductToEdit}/>
			</div>
		);
	}

	private handleChange(event: React.FormEvent<HTMLInputElement>): void {
		const target: HTMLInputElement = event.target as HTMLInputElement;
		const value: string = target.value;

		clearTimeout(this.onChangeTimeout);

		this.onChangeTimeout = setTimeout((): void => {
			if (this.isQuerable(value)) {
				this.props.onSearchProduct(value);
			}
		}, 300);
	}

	private isQuerable(value: string): boolean {
		return value.length >= MIN_QUERY_LENGTH;
	}

}
