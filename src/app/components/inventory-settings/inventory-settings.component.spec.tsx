import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { expect } from 'chai';

import { InventorySettingsComponent, InventorySettingsProps, } from './inventory-settings.component';
import { Inventory } from '../../../domain';
import { response } from '../../fixtures/inventory';

describe('Component: inventory settings', () => {
	let wrapper: ReactWrapper<{}, {}>;
	let props: InventorySettingsProps;

	beforeEach(() => {
		const inventory: Inventory = new Inventory(response.inventory);

		props = {
			settings: inventory.settings,
			onSelectInventorySettingsToEdit: () => void {}
		};

		wrapper = mount(<InventorySettingsComponent {...props} />);
	});

	it('should have company name', () => {
		expect(wrapper.find('.company-name')).to.have.text('Sklep spożywczy');
	});

	it('should have company address', () => {
		expect(wrapper.find('.company-address')).to.have.text('ul. 3 Maja 30, Katowice');
	});

	it('should have committee info', () => {
		expect(wrapper.find('.committee').text()).to.contain('Jan Kowalski, Andrzej Nowak');
	});

	it('should have others people info', () => {
		expect(wrapper.find('.others').text()).to.contain('Anna Kowalska');
	});
});
