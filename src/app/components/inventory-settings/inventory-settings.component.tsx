import * as React from 'react';
import { InventorySettings } from '../../../domain';

import './inventory-settings.component.styl';

export type InventorySettingsProps = {
	settings: InventorySettings,
	onSelectInventorySettingsToEdit(): void
}

export class InventorySettingsComponent extends React.Component<InventorySettingsProps, void> {

	render(): JSX.Element {
		return (
			<div id="inventory-settings">
				{ this.props.settings ? (
					<ul onClick={this.props.onSelectInventorySettingsToEdit.bind(this)}>
						<li>
							<span className="text">
								<span className="company-name">{this.props.settings.companyName}</span>,&nbsp;
								<span className="company-address">{this.props.settings.companyAddress}</span>
							</span>
						</li>
						<li>
							<span className="text">
								<span className="committee">Skład komisji: {this.props.settings.committee}</span>
								{ this.props.settings.others ? (
									<span className="others">&nbsp;|&nbsp; Osoby obecne przy spisie: {this.props.settings.others}</span>
								) : null}
							</span>
							<span className="glyphicon glyphicon-pencil icon-pencil"></span>
						</li>
					</ul>
				) : (
					<button type="button" className="btn btn-default" aria-label="Left Align" onClick={this.props.onSelectInventorySettingsToEdit.bind(this)}>
						<span className="glyphicon glyphicon-pencil" aria-hidden="true"></span> Uzupełnij dane spisu
					</button>
				)}
			</div>
		);
	}

}
