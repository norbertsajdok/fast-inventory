import * as React from 'react';

const selectInputText: (element: HTMLInputElement) => void = (element: HTMLInputElement): void => {
	element.setSelectionRange(0, element.value.length);
};

type FieldProps = {
	text: string,
	param: string,
	onChange: Function,
	placeholder?: string,
	staticClassName?: string,
	editingClassName?: string,
	staticElement?: string,
	editingElement?: string,
	minLength?: number,
	maxLength?: number,
	tabIndex?: number,
	editing?: boolean,
	validate?(text: string): boolean
}

type FieldState = {
	text: string,
	editing: boolean
}

export class FieldEdit extends React.Component<FieldProps, FieldState> {
	static defaultProps: FieldProps = {
		text: '',
		param: '',
		onChange: () => {},
		staticClassName: 'static-cell',
		editingClassName: 'editing-cell',
		staticElement: 'span',
		editingElement: 'input',
		minLength: 1,
		maxLength: 256,
		tabIndex: 0,
		editing: false
	};

	state: FieldState = {
		text: this.props.text,
		editing: this.props.editing || false
	};

	private input: HTMLInputElement;

	componentWillMount(): void {
		this.isInputValid = this.props.validate || this.isInputValid;
	}

	componentWillReceiveProps(nextProps: FieldProps): void {
		const isTextChanged: boolean = (nextProps.text !== this.props.text);
		const isEditingChanged: boolean = (nextProps.editing !== this.props.editing);
		const nextState: any = {};

		if (isTextChanged) {
			nextState.text = nextProps.text;
		}

		if (isEditingChanged) {
			nextState.editing = nextProps.editing;
		}

		if (isTextChanged || isEditingChanged) {
			this.setState(Object.assign({}, this.state, nextState));
		}
	}

	componentDidUpdate(prevProps: FieldProps, prevState: FieldState): void {
		if (this.state.editing && !prevState.editing) {
			this.input.focus();
			this.input.select();
			selectInputText(this.input);
		}
		else if (this.state.editing && prevProps.text !== this.props.text) {
			this.finishEditing();
		}
	}

	render(): JSX.Element {
		if (!this.state.editing) {
			const Element: string = this.props.staticElement || '';
			return (
				<div className="cell-to-edit" onClick={this.startEditing.bind(this)}>
					<Element
						className={this.props.staticClassName}
						tabIndex={this.props.tabIndex}>
						{this.state.text || this.props.placeholder}
					</Element>
					<span className="glyphicon glyphicon-pencil icon-pencil"></span>
				</div>
			);
		}
		else {
			const Element: string = this.props.editingElement || '';
			return <Element
				onClick={this.clickWhenEditing.bind(this)}
				onKeyDown={this.keyDown.bind(this)}
				onBlur={this.finishEditing.bind(this)}
				onChange={this.textChanged.bind(this)}
				className={this.props.editingClassName}
				placeholder={this.props.placeholder}
				defaultValue={this.state.text}
				ref={(input: HTMLInputElement) => this.input = input} />;
		}
	}

	private startEditing(event: Event): void {
		event.stopPropagation();

		this.setState(Object.assign({}, this.state, {
			text: this.props.text,
			editing: true
		}));
	}

	private finishEditing(): void {
		if (this.isInputValid(this.state.text) && this.props.text !== this.state.text) {
			this.commitEditing();
		}
		else if (this.props.text === this.state.text || !this.isInputValid(this.state.text)) {
			this.cancelEditing();
		}
	}

	private cancelEditing(): void {
		this.setState(Object.assign({}, this.state, {
			text: this.props.text,
			editing: false
		}));
	}

	private commitEditing(): void {
		this.setState(Object.assign({}, this.state, {
			text: this.state.text,
			editing: false
		}));

		this.props.onChange({[this.props.param]: this.state.text});
	}

	private clickWhenEditing(event: Event): void {
		event.stopPropagation();
	}

	private isInputValid(text: string): boolean {
		return (text.length >= this.props.minLength && text.length <= this.props.maxLength);
	}

	private keyDown(event: KeyboardEvent): void {
		if (event.keyCode === 13) {
			this.finishEditing();
		}
		else if (event.keyCode === 27) {
			this.cancelEditing();
		}
	}

	private textChanged(event: React.FormEvent<HTMLInputElement>): void {
		const target: HTMLInputElement = event.target as HTMLInputElement;

		this.setState(Object.assign({}, this.state, {
			text: target.value.trim()
		}));
	}
}
