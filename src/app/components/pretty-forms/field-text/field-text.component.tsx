import * as React from 'react';
import { PrettyFormField } from '../pretty-form/pretty-form.component';

type FieldProps = {
	name: string,
	type?: string,
	value?: string,
	required?: boolean,
	label?: string,
	onChange: Function,
	errorMessage?: string
}

type FieldState = {
	value: string,
	hasError: boolean,
}

export class FieldText extends React.Component<FieldProps, FieldState> implements PrettyFormField {
	static contextTypes: any = {
		register: React.PropTypes.func.isRequired
	};

	static defaultProps: any = {
		type: 'text',
		value: '',
		required: false,
		label: '',
		errorMessage: ''
	};

	state: FieldState = {
		value: this.props.value || '',
		hasError: false
	};

	componentWillMount(): void {
		this.context.register(this);
	}

	componentWillReceiveProps(nextProps: FieldProps): void {
		this.setState(Object.assign({}, this.state, {value: nextProps.value}));
	}

	render(): JSX.Element {
		return (
			<div className="form-group">
				{ this.props.label ? (
					<label htmlFor={this.props.name}>{this.props.label}:</label>
				) : null}
				<input type={this.props.type}
						name={this.props.name}
						className={`form-control ${this.state.hasError ? 'error' : ''}`}
						id={this.props.name}
						placeholder={this.props.label || ''}
						value={this.state.value}
						onChange={this.handleChange.bind(this)}
						onBlur={this.handleBlur.bind(this)}
						onKeyUp={this.handleKeyUp.bind(this)}/>
				{ this.state.hasError ? (
					<span className="error-msg">{this.props.errorMessage}</span>
				) : null}
			</div>
		)
	}

	isValid(): boolean {
		return !this.state.hasError;
	}

	validate(): boolean {
		if (this.props.required) {
			if (!this.state.value) {
				this.setState(Object.assign({}, this.state, {hasError: true}));
				return false;
			}

			this.setState(Object.assign({}, this.state, {hasError: false}));
			return true;
		}

		return true;
	}

	reset(): void {
		this.setState(Object.assign({}, this.state, {
			value: '',
			hasError: false
		}));
	}

	private handleChange(event: React.FormEvent<HTMLInputElement>): void {
		const target: HTMLInputElement = event.target as HTMLInputElement;

		this.setState(Object.assign({}, this.state, {value: target.value}));

		this.props.onChange(target.name, target.value);
	}

	private handleBlur(): void {
		this.validate();
	}

	private handleKeyUp(event: React.KeyboardEvent<HTMLInputElement>): void {
		const keyCode: number = event.keyCode || event.which;

		switch (keyCode) {
			case 9: // tab
			case 16: // shift
			case 18: // left alt
			case 17: // right alt
				// do nothing
				break;
			default:
				if (this.state.hasError) {
					this.setState(Object.assign({}, this.state, {hasError: false}));
				}
				break;
		}
	}
}
