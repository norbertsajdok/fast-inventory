import * as React from 'react';

import './pretty-form.component.styl';

type PrettyFormProps = {
	onSubmit: Function
}

type PrettyFormChildContextTypes = {
	register(field: PrettyFormField): void
}

export interface PrettyFormField {
	isValid(): boolean,
	validate(): boolean,
	reset(): void
}

export class PrettyForm extends React.Component<PrettyFormProps, void> {
	static childContextTypes: any = {
		register: React.PropTypes.func.isRequired
	};

	private fields: PrettyFormField[];

	componentWillMount(): void {
		this.fields = [];
	}

	getChildContext(): PrettyFormChildContextTypes {
		return {
			register: this.registerField.bind(this)
		};
	}

	render(): JSX.Element {
		return (
			<form className="pretty-form" onSubmit={this.handleSubmit.bind(this)}>
				{this.props.children}
			</form>
		)
	}

	private registerField(field: PrettyFormField): void {
		if (this.fields.indexOf(field) === -1) {
			this.fields.push(field);
		}
	}

	private handleSubmit(event: React.FormEvent<HTMLFormElement>): void {
		event.preventDefault();

		this.fields.forEach((field: PrettyFormField): boolean => field.validate());

		const formIsValid: boolean = this.fields.every((field: PrettyFormField): boolean => {
			return field.isValid();
		});

		if (formIsValid) {
			this.props.onSubmit();
			this.fields.forEach((field: PrettyFormField): void => field.reset());
		}
	}
}
