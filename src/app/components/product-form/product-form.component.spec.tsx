import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { expect } from 'chai';
import * as sinon from 'sinon';

import { ProductFormComponent, ProductFormProps } from './product-form.component';

const fields: any = {
	name: 'input[name="name"]',
	unit: 'select[name="unit"]',
	quantity: 'input[name="quantity"]',
	net_price: 'input[name="net_price"]',
	submit: 'button[type="submit"]'
};

describe('Component: product form', () => {
	let wrapper: ReactWrapper<{}, {}>;
	let props: ProductFormProps;

	beforeEach(() => {
		props = {
			onAddProduct: sinon.spy()
		};

		wrapper = mount(<ProductFormComponent {...props} />);
	});

	it('should have initial state', () => {
		expect(wrapper.state()).to.deep.equal({
			name: '',
			unit: 'szt.',
			quantity: '',
			net_price: '',
			submitted: false
		});
	});

	it('should have a product name input', () => {
		expect(wrapper.find(fields.name)).to.have.length(1);
	});

	it('should have a product unit select', () => {
		expect(wrapper.find(fields.unit)).to.have.length(1);
	});

	it('should have a product quantity input', () => {
		expect(wrapper.find(fields.quantity)).to.have.length(1);
	});

	it('should have a product net price input', () => {
		expect(wrapper.find(fields.net_price)).to.have.length(1);
	});

	it('should have a submit button', () => {
		expect(wrapper.find(fields.submit)).to.have.length(1);
	});

	describe('on change', () => {
		it('should set state for name', () => {
			wrapper.find(fields.name).simulate('change', {target: {name: 'name', value: 'Product name'}});

			expect(wrapper.state('name')).to.deep.equal('Product name')
		});

		it('should set state for unit', () => {
			wrapper.find(fields.unit).simulate('change', {target: {name: 'unit', value: 'Product unit'}});

			expect(wrapper.state('unit')).to.deep.equal('Product unit')
		});

		it('should set state for quantity', () => {
			wrapper.find(fields.quantity).simulate('change', {target: {name: 'quantity', value: 'Product quantity'}});

			expect(wrapper.state('quantity')).to.deep.equal('Product quantity')
		});

		it('should set state for net price', () => {
			wrapper.find(fields.net_price).simulate('change', {target: {name: 'net_price', value: 'Product net price'}});

			expect(wrapper.state('net_price')).to.deep.equal('Product net price')
		});
	});

	describe('on submit', () => {
		beforeEach(() => {
			// set value for required fields
			wrapper.find(fields.name).simulate('change', {target: {name: 'name', value: 'Product name'}});
			wrapper.find(fields.unit).simulate('change', {target: {name: 'unit', value: 'Product unit'}});
		});

		it('should dispatch addProduct action', () => {
			const spy: any = props.onAddProduct;
			wrapper.find('form').simulate('submit');
			expect(spy.calledOnce).to.equal(true);
		});

		it('should set state for submitted', () => {
			wrapper.find('form').simulate('submit');

			expect(wrapper.state('submitted')).to.deep.equal(true);
		});
	});

});
