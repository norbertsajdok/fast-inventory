import * as React from 'react';
import { PrettyForm, FieldText, FieldSelect, SelectOption  } from '../pretty-forms';
import { Sticky } from '../../utils/sticky.class';

import './product-form.component.styl';

export type ProductFormProps = {
	onAddProduct(data: any): void;
}

type ProductFormState = {
	name: string;
	unit: string;
	quantity: string;
	net_price: string;
	submitted: boolean
}

const unitSelectOptions: SelectOption[] = [{
	label: 'Sztuki',
	value: 'szt.'
}, {
	label: 'Kilogramy',
	value: 'kg.'
}];

export class ProductFormComponent extends React.Component<ProductFormProps, ProductFormState> {
	state: ProductFormState = {
		name: '',
		unit: 'szt.',
		quantity: '',
		net_price: '',
		submitted: false
	};

	private stickyElement: HTMLElement;
	private placeholderElement: HTMLElement;

	componentDidMount(): void {
		new Sticky(this.stickyElement, this.placeholderElement);
	}

	componentWillReceiveProps(): void {
		if (this.state.submitted) {
			this.setState(Object.assign({}, this.state, {
				name: '',
				unit: 'szt.',
				quantity: '',
				net_price: '',
				submitted: false
			}));
		}
	}

	render(): JSX.Element {
		return (
			<li className="matrix-row" id="product-form">
				<div className="placeholder-element hidden" ref={(div: HTMLElement) => this.placeholderElement = div}></div>
				<div className="sticky-element" ref={(div: HTMLElement) => this.stickyElement = div}>
					<PrettyForm onSubmit={this.handleSubmit.bind(this)}>
						<ul>
							<li className="cell id">Nowy</li>
							<li className="cell">
								<FieldText name="name" value={this.state.name} required
											onChange={this.handleChange.bind(this)} errorMessage="Pole jest wymagane"/>
							</li>
							<li className="cell">
								<FieldSelect name="unit" value={this.state.unit} required options={unitSelectOptions}
											onChange={this.handleChange.bind(this)} errorMessage="Pole jest wymagane"/>
							</li>
							<li className="cell">
								<FieldText name="quantity" value={this.state.quantity} onChange={this.handleChange.bind(this)} />
							</li>
							<li className="cell">
								<FieldText name="net_price" value={this.state.net_price} onChange={this.handleChange.bind(this)} />
							</li>
							<li className="cell">
								<button type="submit" className="btn btn-default" aria-label="Left Align">
									<span className="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> dodaj
								</button>
							</li>
						</ul>
					</PrettyForm>
				</div>
			</li>
		);
	}

	private handleChange(name: string, value: string): void {
		this.setState(Object.assign({}, this.state, {[name]: value}));
	}

	private handleSubmit(): void {
		this.props.onAddProduct(this.state);
		this.setState(Object.assign({}, this.state, {submitted: true}));
	}
}
