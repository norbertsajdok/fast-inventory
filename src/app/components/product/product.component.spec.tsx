import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { expect } from 'chai';
import * as sinon from 'sinon';

import { ProductComponent, ProductProps } from './product.component';
import { Inventory } from '../../../domain';
import { response } from '../../fixtures/inventory';

describe('Component: product', () => {
	let wrapper: ReactWrapper<{}, {}>;
	let props: ProductProps;

	beforeEach(() => {
		const inventory: Inventory = new Inventory(response.inventory);

		props = {
			index: 1,
			product: inventory.products[0],
			onEditProduct: sinon.spy(),
			onRemoveProduct: sinon.spy(),
		};

		wrapper = mount(<ProductComponent {...props} />);
	});

	it('should have product index', () => {
		expect(wrapper.find('.index')).to.have.text('2');
	});

	it('should have product name', () => {
		expect(wrapper.find('.name').text()).to.contain(props.product.name);
	});

	it('should have product unit', () => {
		expect(wrapper.find('.unit').text()).to.contain(props.product.unit);
	});

	it('should have product quantity', () => {
		expect(wrapper.find('.quantity').text()).to.contain(props.product.quantity);
	});

	it('should have product net price', () => {
		expect(wrapper.find('.net_price').text()).to.contain(props.product.net_price.toString());
	});

	it('should have product net value', () => {
		expect(wrapper.find('.net_value')).to.have.text(props.product.net_value.toString());
	});

	it('should dispatch remove product action', () => {
		const spy: any = props.onRemoveProduct;
		wrapper.find('button[type="button"]').simulate('click');
		expect(spy.calledOnce).to.equal(true);
	});
});
