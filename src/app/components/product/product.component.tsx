import * as React from 'react';
import { Product } from '../../../domain';
import { FieldEdit } from '../pretty-forms';

import './product.component.styl';

export type ProductProps = {
	index: number;
	product: Product;
	onEditProduct(product: Product, data: any): void;
	onRemoveProduct(product: Product): void;
}

export class ProductComponent extends React.Component<ProductProps, void> {

	render(): JSX.Element {
		return (
			<li className="matrix-row product">
				<ul>
					<li className="cell index">{this.props.index + 1}</li>
					<li className="cell name">
						<FieldEdit
							text={this.props.product.name.toString()}
							onChange={this.handleClickToEdit.bind(this)}
							param="name"
						/>
					</li>
					<li className="cell unit">
						<FieldEdit
							text={this.props.product.unit.toString()}
							onChange={this.handleClickToEdit.bind(this)}
							param="unit"
						/>
					</li>
					<li className="cell quantity">
						<FieldEdit
							text={this.props.product.quantity.toString()}
							onChange={this.handleClickToEdit.bind(this)}
							param="quantity"
							validate={this.validateNumber.bind(this)}
						/>
					</li>
					<li className="cell net_price">
						<FieldEdit
							text={this.props.product.net_price.toString()}
							onChange={this.handleClickToEdit.bind(this)}
							param="net_price"
							validate={this.validatePrice.bind(this)}
						/>
					</li>
					<li className="cell net_value">{this.props.product.net_value.toString()}</li>
					<li className="cell product-remover">
						<button type="button" className="btn btn-default" aria-label="Left Align" onClick={this.handleClickToRemove.bind(this)}>
							<span className="glyphicon glyphicon-minus-sign" aria-hidden="true"></span> usuń
						</button>
					</li>
				</ul>
			</li>
		);
	}

	private handleClickToEdit(data: Object): void {
		this.props.onEditProduct(this.props.product, data);
	}

	private handleClickToRemove(): void {
		this.props.onRemoveProduct(this.props.product);
	}

	private validateNumber(text: string): boolean {
		return !isNaN(+text);
	}

	private validatePrice(text: string): boolean {
		const number: number = +(text.replace(this.props.product.net_price.currency, ''));
		return !isNaN(number);
	}
}
