import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { expect } from 'chai';

import { ProductsListComponent, ProductsListProps } from './products-list.component';
import { Inventory, Product } from '../../../domain';
import { response } from '../../fixtures/inventory';

describe('Component: products list', () => {
	let wrapper: ReactWrapper<{}, {}>;
	let props: ProductsListProps;

	beforeEach(() => {
		const inventory: Inventory = new Inventory(response.inventory);

		props = {
			products: inventory.products,
			onAddProduct: (data: any): void => {},
			onEditProduct: (product: Product, data: any): void => {},
			onRemoveProduct: (product: Product): void => {},
		};

		wrapper = mount(<ProductsListComponent {...props} />);
	});

	it('should have product form component', () => {
		expect(wrapper.find('#product-form').length).to.equal(1);
	});

	it('should have products list', () => {
		expect(wrapper.find('.product').length).to.equal(6);
	});

	describe('when products is empty', () => {
		beforeEach(() => {
			props = Object.assign({}, props, {
				products: []
			});

			wrapper = mount(<ProductsListComponent {...props} />);
		});

		it('should have products list', () => {
			expect(wrapper.find('.product').length).to.equal(0);
		});

		it('should display products nof found info', () => {
			expect(wrapper.find('#products-not-found').length).to.equal(1);
		});
	});
});
