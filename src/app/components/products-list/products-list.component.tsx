import * as React from 'react';
import { Dispatch } from 'redux';
import { connect, MapStateToProps, MapDispatchToPropsFunction } from 'react-redux';
import { Product } from '../../../domain';
import { ProductComponent } from '../product/product.component';
import { ProductFormComponent } from '../product-form/product-form.component';
import {
	createAddProductAction,
	createEditProductAction,
	createRemoveProductAction
} from '../../actions/inventory.actions';
import { State } from '../../state';

import './products-list.component.styl';

export type ProductsListProps = {
	products: Product[];
	onAddProduct(data: any): void;
	onEditProduct(product: Product, data: any): void;
	onRemoveProduct(product: Product): void;
}

export class ProductsListComponent extends React.Component<ProductsListProps, void> {

	render(): JSX.Element {
		return (
			<ul className="matrix" id="products-list">
				<li className="matrix-row" id="products-list-header">
					<ul>
						<li className="cell">id</li>
						<li className="cell">nazwa</li>
						<li className="cell">j.m</li>
						<li className="cell">ilość</li>
						<li className="cell">cena netto</li>
						<li className="cell">wartość netto</li>
					</ul>
				</li>
				<ProductFormComponent onAddProduct={this.props.onAddProduct}/>
				{this.props.products.length ? (
					this.props.products.map((product: Product, index: number): JSX.Element => {
						return <ProductComponent
							key={index}
							index={index}
							product={product}
							onEditProduct={this.props.onEditProduct}
							onRemoveProduct={this.props.onRemoveProduct}
						/>
					})
				) : (
					<li className="matrix-row" id="products-not-found">
						<h3>Nie masz jeszcze wprowadzonych produktów do spisu.</h3>
						<h3>Wprowadź pierwszy produkt.</h3>
					</li>
				)}
			</ul>
		);
	}
}

const mapStateToProps: MapStateToProps<{}, {}> = (state: State): Object => {
	return {
		products: state.inventory.products
	}
};

const mapDispatchToProps: MapDispatchToPropsFunction<{}, {}> = (dispatch: Dispatch<any>): Object => {
	return {
		onAddProduct: (data: any): void => {
			dispatch(createAddProductAction(data));
		},
		onEditProduct: (product: Product, data: any): void => {
			dispatch(createEditProductAction(product, data));
		},
		onRemoveProduct: (product: Product): void => {
			dispatch(createRemoveProductAction(product))
		}
	}
};

export const ConnectedProductsListComponent: React.ComponentClass<{}> = connect(mapStateToProps, mapDispatchToProps)(ProductsListComponent);
