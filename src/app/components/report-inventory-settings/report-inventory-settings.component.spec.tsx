import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { expect } from 'chai';

import { ReportInventorySettingsComponent, ReportInventorySettingsProps } from './report-inventory-settings.component';
import { Inventory } from '../../../domain';
import { response } from '../../fixtures/inventory';

describe('Component: report inventory settings', () => {
	let wrapper: ShallowWrapper<{}, {}>;
	let props: ReportInventorySettingsProps;

	beforeEach(() => {
		const inventory: Inventory = new Inventory(response.inventory);

		props = {
			settings: inventory.settings
		};

		wrapper = shallow(<ReportInventorySettingsComponent {...props} />);
	});

	it('should have company name', () => {
		expect(wrapper.find('.company-name')).to.have.text('Sklep spożywczy');
	});

	it('should have company address', () => {
		expect(wrapper.find('.company-address')).to.have.text('ul. 3 Maja 30, Katowice');
	});

	it('should have committee info', () => {
		expect(wrapper.find('.committee')).to.have.text('Jan Kowalski, Andrzej Nowak');
	});

	it('should have others people info', () => {
		expect(wrapper.find('.others')).to.have.text('Anna Kowalska');
	});

	it('should have start date', () => {
		expect(wrapper.find('.start-date')).to.have.text('24 December 2016, 12:25');
	});

	it('should have end date', () => {
		expect(wrapper.find('.end-date')).to.have.text('24 December 2016, 14:25');
	});
});
