import * as React from 'react';
import * as moment from 'moment';
import { InventorySettings } from '../../../domain';

import './report-inventory-settings.component.styl';

export type ReportInventorySettingsProps = {
	settings: InventorySettings
}

export class ReportInventorySettingsComponent extends React.Component<ReportInventorySettingsProps, void> {

	render(): JSX.Element {
		return (
			<div className="col-md-3 inventory-settings">
				<ul>
					<li>
						<span className="company-name">{this.props.settings.companyName}</span> <br />
						<span className="company-address">{this.props.settings.companyAddress}</span>
					</li>
					<li className="stamp">pieczęć firmy</li>
					<li>
						<b>Skład komisji inwentaryzacyjnej:</b> <br />
						<span className="committee">{this.props.settings.committee}</span>
					</li>
					{ this.props.settings.others ? (
						<li>
							<b>Osoby obecne przy spisie:</b> <br />
							<span className="others">{this.props.settings.others}</span>
						</li>
					) : null }
					{ this.props.settings.startDate ? (
						<li>
							<b>Data rozpoczęcia spisu:</b> <br />
							<span className="start-date">{this.formatDate(this.props.settings.startDate)}</span>
						</li>
					) : null }
					{ this.props.settings.endDate ? (
						<li>
							<b>Data zakończenia spisu:</b> <br />
							<span className="end-date">{this.formatDate(this.props.settings.endDate)}</span>
						</li>
					) : null }
				</ul>
			</div>
		);
	}

	private formatDate(date: string): string {
		return moment(date).format('DD MMMM YYYY, HH:mm');
	}
}
