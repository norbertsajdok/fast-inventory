import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { expect } from 'chai';

import { ReportPageComponent, ReportPageProps } from './report-page.component';
import { Inventory } from '../../../domain';
import { response } from '../../fixtures/inventory';

describe('Component: report page', () => {
	let wrapper: ReactWrapper<{}, {}>;
	let props: ReportPageProps;

	beforeEach(() => {
		const inventory: Inventory = new Inventory(response.inventory);

		props = {
			pageNumber: 1,
			pagesCount: 2,
			products: inventory.products,
			settings: inventory.settings
		};

		wrapper = mount(<ReportPageComponent {...props} />);
	});

	it('should have page counter', () => {
		expect(wrapper.find('.page-count')).to.have.text('strona 1 z 2');
	});

	it('should have products list', () => {
		expect(wrapper.find('.report-product').length).to.equal(6);
	});

	it('should have page value', () => {
		expect(wrapper.find('.page-value').text()).to.contain('290.49 PLN');
	});

	it('should have page value in words', () => {
		expect(wrapper.find('.page-value-in-words').text()).to.contain('dwieście dziewięćdziesiąt zł. czterdzieści dziewięć gr.');
	});
});
