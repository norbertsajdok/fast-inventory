import * as React from 'react';
import { Product, Price, InventorySettings } from '../../../domain';
import { ReportProductComponent } from '../report-product/report-product.component';
import { PRODUCTS_PER_PAGE } from '../report/report.component';
import { numberToWords } from '../../utils/numberToWords';

import './report-page.component.styl';
import { ReportInventorySettingsComponent } from '../report-inventory-settings/report-inventory-settings.component';

export type ReportPageProps = {
	pageNumber: number;
	pagesCount: number;
	products: Product[];
	settings: InventorySettings;
}

export class ReportPageComponent extends React.Component<ReportPageProps, void> {

	render(): JSX.Element {
		return (
			<li className="report-page">
				<div className="row">
					<div className="col-md-12 page-header">
						<h3>Arkusz spisu z natury</h3>
						<span className="page-count">strona {this.props.pageNumber} z {this.props.pagesCount}</span>
					</div>
				</div>
				<div className="row">
					<ReportInventorySettingsComponent settings={this.props.settings} />
					<ul className="col-md-9 matrix">
						<li className="matrix-row">
							<ul>
								<li className="cell">lp</li>
								<li className="cell">nazwa</li>
								<li className="cell">j.m</li>
								<li className="cell">ilość</li>
								<li className="cell">cena netto</li>
								<li className="cell">wartość netto</li>
							</ul>
						</li>
						{this.props.products.map((product: Product, index: number): JSX.Element => {
							return <ReportProductComponent
								key={index}
								index={(this.props.pageNumber * PRODUCTS_PER_PAGE) - PRODUCTS_PER_PAGE + index + 1}
								product={product}
							/>
						})}
					</ul>
				</div>
				<div className="row">
					<div className="col-md-12 page-footer">
						<div className="col-md-3 signature">
							<span>podpis</span>
						</div>
						<table>
							<tbody>
								<tr>
									<td>wartość strony:</td>
									<td className="page-value">{this.getProductsValue().toString()}</td>
								</tr>
								<tr>
									<td>wartość strony słownie:</td>
									<td className="page-value-in-words">{this.getProductValueInWords()}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</li>
		);
	}

	private getProductsValue(): Price {
		let productsValue: number = 0;

		this.props.products.forEach((product: Product) => {
			productsValue += product.net_value.amount;
		});

		return new Price({amount: productsValue});
	}

	private getProductValueInWords(): string {
		const price: string[] = this.getProductsValue().amount.toString().split('.');

		return `${numberToWords(+price[0])} zł. ${numberToWords(+price[1])} gr.`;
	}
}
