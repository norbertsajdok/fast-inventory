import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { expect } from 'chai';

import { ReportProductComponent, ReportProductProps } from './report-product.component';
import { Product } from '../../../domain';

describe('Component: report product', () => {
	let wrapper: ShallowWrapper<{}, {}>;
	let props: ReportProductProps;

	beforeEach(() => {
		props = {
			index: 1,
			product: new Product({
				id: 'uuid-placeholer',
				name: 'product name',
				unit: 'product unit',
				quantity: 3.75,
				net_price: {
					amount: 5.25
				}
			})
		};

		wrapper = shallow(<ReportProductComponent {...props} />);
	});

	it('should contain all product data', () => {
		expect(wrapper).to.have.exactly(6).descendants('.cell');
	});

	it('should contain index of product', () => {
		expect(wrapper.find('.cell').first()).to.have.text('1');
	});

	it('should contain product name', () => {
		expect(wrapper.find('.product-name')).to.have.text(props.product.name);
	});

	it('should contain product unit', () => {
		expect(wrapper.find('.product-unit')).to.have.text(props.product.unit);
	});

	it('should contain product quantity', () => {
		expect(wrapper.find('.product-quantity')).to.have.text(props.product.quantity.toString());
	});

	it('should contain product net price', () => {
		expect(wrapper.find('.product-net-price')).to.have.text(props.product.net_price.toString());
	});

	it('should contain product net value', () => {
		expect(wrapper.find('.product-net-value')).to.have.text(props.product.net_value.toString());
	});
});
