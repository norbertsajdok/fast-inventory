import * as React from 'react';
import { Product } from '../../../domain';

export type ReportProductProps = {
	index: number;
	product: Product;
}

export class ReportProductComponent extends React.Component<ReportProductProps, void> {

	render(): JSX.Element {
		return (
			<li className="matrix-row report-product">
				<ul>
					<li className="cell">{this.props.index}</li>
					<li className="cell product-name">{this.props.product.name}</li>
					<li className="cell product-unit">{this.props.product.unit}</li>
					<li className="cell product-quantity">
						{this.props.product.quantity > 0 ? (
							this.props.product.quantity.toString()
						) : (
							<span>&nbsp;</span>
						) }
					</li>
					<li className="cell product-net-price">
						{this.props.product.net_price.amount > 0 ? (
							this.props.product.net_price.toString()
						) : (
							<span>&nbsp;</span>
						) }
					</li>
					<li className="cell product-net-value">
						{this.props.product.net_value.amount > 0 ? (
							this.props.product.net_value.toString()
						) : (
							<span>&nbsp;</span>
						) }
					</li>
				</ul>
			</li>
		);
	}
}
