import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { expect } from 'chai';

import { ReportSummaryPageComponent, ReportSummaryPageProps } from './report-summary-page.component';
import { Inventory, Product, FIProduct } from '../../../domain';
import { response } from '../../fixtures/inventory';

const getPages: (inventory: Inventory) => Product[][] = (inventory: Inventory): Product[][] => {
	const products: Product[] = inventory.products.map((item: FIProduct) => new Product(item));

	return [products, products]
};

describe('Component: report summary page', () => {
	let wrapper: ShallowWrapper<{}, {}>;
	let props: ReportSummaryPageProps;

	beforeEach(() => {
		const inventory: Inventory = new Inventory(response.inventory);

		props = {
			pages: getPages(inventory),
			settings: inventory.settings
		};

		wrapper = shallow(<ReportSummaryPageComponent {...props} />);
	});

	it('should have page counter', () => {
		expect(wrapper.find('.page-count')).to.have.text('strona 3 z 3');
	});

	it('should have list of pages summary', () => {
		expect(wrapper.find('.page-summary').length).to.equal(2);
	});

	it('should have inventory value', () => {
		expect(wrapper.find('.inventory-value').text()).to.contain('580.98 PLN');
	});

	it('should have inventory value in words', () => {
		expect(wrapper.find('.inventory-value-in-words').text()).to.contain('pięćset osiemdziesiąt zł. dziewięćdziesiąt osiem gr.');
	});

	it('should have inventory products count', () => {
		expect(wrapper.find('.inventory-products-count')).to.have.text('12');
	});
});
