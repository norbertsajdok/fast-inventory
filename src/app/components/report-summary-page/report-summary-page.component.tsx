import * as React from 'react';
import { Product, Price, InventorySettings } from '../../../domain';
import { numberToWords } from '../../utils/numberToWords';

import './report-summary-page.component.styl';
import { ReportInventorySettingsComponent } from '../report-inventory-settings/report-inventory-settings.component';

export type ReportSummaryPageProps = {
	pages: Product[][],
	settings: InventorySettings;
}

export class ReportSummaryPageComponent extends React.Component<ReportSummaryPageProps, void> {
	private pagesPerColumn: number;

	render(): JSX.Element {
		return (
			<li className="report-summary-page">
				<div className="row">
					<div className="col-md-12 page-header">
						<h3>Arkusz spisu z natury</h3>
						<span className="page-count">strona {this.props.pages.length + 1} z {this.props.pages.length + 1}</span>
					</div>
				</div>
				<div className="row">
					<ReportInventorySettingsComponent settings={this.props.settings} />
					<div className="col-md-9">
						{this.getColumnsOfPages().map((columnOfPages: Product[][], column: number): JSX.Element => {
							return (
								<ul className="matrix" key={column}>
									<li className="matrix-row">
										<ul>
											<li className="cell">Numer strony</li>
											<li className="cell">Wartość strony netto</li>
										</ul>
									</li>
									{columnOfPages.map((products: Product[], page: number): JSX.Element => {
										return (
											<li className="matrix-row page-summary" key={page}>
												<ul>
													<li className="cell">{(column * this.pagesPerColumn) + page + 1}</li>
													<li className="cell">{this.getPageValue(products).toString()}</li>
												</ul>
											</li>
										)
									})}
								</ul>
							)
						})}
					</div>
				</div>
				<div className="row">
					<div className="col-md-12 page-footer">
						<div className="col-md-3 signature">
							<span>podpis</span>
						</div>
						<table>
							<tbody>
								<tr>
									<td>wartość spisu:</td>
									<td className="inventory-value">{this.getInventoryValue().toString()}</td>
								</tr>
								<tr>
									<td>wartość spisu słownie:</td>
									<td className="inventory-value-in-words">{this.getInventoryValueInWords()}</td>
								</tr>
								<tr>
									<td>spis zakończono na pozycji:</td>
									<td className="inventory-products-count">{this.getProductsCount()}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</li>
		);
	}

	private getPageValue(products: Product[]): Price {
		let productsValue: number = 0;

		products.forEach((product: Product): void => {
			productsValue += product.net_value.amount;
		});

		return new Price({amount: productsValue});
	}

	private getInventoryValue(): Price {
		let inventoryValue: number = 0;

		this.props.pages.forEach((products: Product[]): void => {
			inventoryValue += this.getPageValue(products).amount
		});

		return new Price({amount: inventoryValue});
	}

	private getInventoryValueInWords(): string {
		const price: string[] = this.getInventoryValue().amount.toString().split('.');

		return `${numberToWords(+price[0])} zł. ${numberToWords(+price[1])} gr.`;
	}

	private getProductsCount(): number {
		let productsCount: number = 0;

		this.props.pages.forEach((products: Product[]): void => {
			productsCount += products.length;
		});

		return productsCount;
	}

	private getColumnsOfPages(): Product[][][] {
		const columns: Product[][][] = [];
		const maxColumnsCount: number = 2;

		if (this.props.pages.length <= 10) {
			this.pagesPerColumn = this.props.pages.length;
		}
		else {
			this.pagesPerColumn = Math.round((this.props.pages.length) / maxColumnsCount);
		}

		columns[0] = this.props.pages.slice(0, this.pagesPerColumn);

		if (this.pagesPerColumn > 10) {
			columns[1] = this.props.pages.slice(this.pagesPerColumn);
		}

		return columns;
	}
}
