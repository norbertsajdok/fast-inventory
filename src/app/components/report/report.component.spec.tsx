import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { expect } from 'chai';
import * as sinon from 'sinon';

import { ReportComponent, ReportProps } from './report.component';
import { Product, FIProduct, Inventory } from '../../../domain';
import { response } from '../../fixtures/inventory';
import { ipcRenderer } from 'electron';

const getPages: (inventory: Inventory) => Product[][] = (inventory: Inventory): Product[][] => {
	const products: Product[] = inventory.products.map((item: FIProduct) => new Product(item));

	return [products, products]
};

describe('Component: report', () => {
	let wrapper: ReactWrapper<{}, {}>;
	let props: ReportProps;

	beforeEach(() => {
		const inventory: Inventory = new Inventory(response.inventory);
		props = {
			pages: getPages(inventory),
			settings: inventory.settings,
			ipc: ipcRenderer || {}
		};

		wrapper = mount(<ReportComponent {...props} />);
	});

	it('should have report pages', () => {
		expect(wrapper.find('.report-page').length).to.equal(2);
	});

	it('should have report summary page', () => {
		expect(wrapper.find('.report-summary-page').length).to.equal(1);
	});

	it('should dispatch action when clicking on print button', () => {
		const spy: any = props.ipc.send = sinon.spy();
		wrapper.find('.print-button').simulate('click');
		expect(spy.calledOnce).to.equal(true);
	});
});
