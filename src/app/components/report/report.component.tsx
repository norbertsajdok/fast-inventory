import * as React from 'react';
import { Link } from 'react-router';
import { connect, MapStateToProps } from 'react-redux';
import { ipcRenderer } from 'electron';
import { State } from '../../state';
import { Product, InventorySettings } from '../../../domain';
import { ReportPageComponent } from '../report-page/report-page.component';
import { ReportSummaryPageComponent } from '../report-summary-page/report-summary-page.component';

import './report.component.styl';

export type ReportProps = {
	pages: Product[][],
	settings: InventorySettings,
	ipc: Electron.IpcRenderer
}

export const PRODUCTS_PER_PAGE: number = 24;

export class ReportComponent extends React.Component<ReportProps, void> {

	render(): JSX.Element {
		return (
			<div>
				<div className="report-nav">
					<Link to="/">
						<span className="glyphicon glyphicon-circle-arrow-left" aria-hidden="true"></span>
						<span className="text">wstecz</span>
					</Link>
					<a href="#" className="print-button" onClick={this.handleClickToPrint.bind(this)}>
						<span className="glyphicon glyphicon-print" aria-hidden="true"></span>
						<span className="text">drukuj</span>
					</a>
				</div>
				<ul id="report-pages">
					{this.props.pages.map((products: Product[], index: number): JSX.Element => {
						return <ReportPageComponent
							key={index}
							pageNumber={index + 1}
							pagesCount={this.props.pages.length + 1}
							products={products}
							settings={this.props.settings}
						/>
					})}
					<ReportSummaryPageComponent pages={this.props.pages} settings={this.props.settings} />
				</ul>
			</div>
		);
	}

	private handleClickToPrint(event: Event): void {
		event.preventDefault();

		this.props.ipc.send('print-to-pdf');
	}

}

const getPages: (products: Product[]) => Product[][] = (products: Product[]): Product[][] =>  {
	const pages: Product[][] = [];
	const pagesCount: number = products.length / PRODUCTS_PER_PAGE;

	for (let i: number = 0; i < pagesCount; i++) {
		const startIndex: number = i * PRODUCTS_PER_PAGE;
		const endIndex: number = startIndex + PRODUCTS_PER_PAGE;

		pages[i] = products.slice(startIndex, endIndex);
	}

	return pages;
};

const mapStateToProps: MapStateToProps<{}, {}> = (state: State): Object => {
	return {
		pages: getPages(state.inventory.products),
		settings: state.inventory.settings,
		ipc: ipcRenderer
	}
};

export const ConnectedReportComponent: React.ComponentClass<{}> = connect(mapStateToProps)(ReportComponent);
