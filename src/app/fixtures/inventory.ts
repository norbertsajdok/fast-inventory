import { InventoryResponse } from '../service/inventory.service';

export const response: InventoryResponse = {
	status: 'done',
	message: 'Response message',
	inventory: {
		products: [
			{
				id: '1',
				name: 'Sprite',
				unit: 'szt.',
				quantity: 14,
				net_price: {
					amount: 3.54,
					currency: 'PLN'
				},
				net_value: {
					amount: 49.56,
					currency: 'PLN'
				}
			},
			{
				id: '2',
				name: 'Mirinda',
				unit: 'szt.',
				quantity: 13,
				net_price: {
					amount: 1.45,
					currency: 'PLN'
				},
				net_value: {
					amount: 18.85,
					currency: 'PLN'
				}
			},
			{
				id: '3',
				name: 'Tymbark',
				unit: 'szt.',
				quantity: 12,
				net_price: {
					amount: 3.13,
					currency: 'PLN'
				},
				net_value: {
					amount: 37.56,
					currency: 'PLN'
				}
			},
			{
				id: '4',
				name: 'Kiwi',
				unit: 'szt.',
				quantity: 8,
				net_price: {
					amount: 4.12,
					currency: 'PLN'
				},
				net_value: {
					amount: 32.96,
					currency: 'PLN'
				}
			},
			{
				id: '5',
				name: 'Cola',
				unit: 'szt.',
				quantity: 9,
				net_price: {
					amount: 4.5,
					currency: 'PLN'
				},
				net_value: {
					amount: 40.5,
					currency: 'PLN'
				}
			},
			{
				id: '6',
				name: 'Pepsi',
				unit: 'szt.',
				quantity: 9,
				net_price: {
					amount: 12.34,
					currency: 'PLN'
				},
				net_value: {
					amount: 111.06,
					currency: 'PLN'
				}
			}
		],
		settings: {
			companyName: 'Sklep spożywczy',
			companyAddress: 'ul. 3 Maja 30, Katowice',
			committee: 'Jan Kowalski, Andrzej Nowak',
			others: 'Anna Kowalska',
			startDate: '2016-12-24T12:25',
			endDate: '2016-12-24T14:25'
		}
	},

};
