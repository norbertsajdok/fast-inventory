import * as React from 'react';
import * as ReactDom from 'react-dom';
import { createStore, applyMiddleware, Middleware } from 'redux';
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';
import { syncHistoryWithStore, routerMiddleware, ReactRouterReduxHistory } from 'react-router-redux';
import * as createLogger from 'redux-logger';
import * as moment from 'moment';
import { State } from './state';
import { reducer } from './reducer/index';
import { inventoryService } from './service/inventory.service';
import { AppComponent } from './components/app/app.component';
import { ConnectedInventoryPage } from './pages/inventory.page';
import { ConnectedPrintPage } from './pages/print.page';
import { inventoryInitialState } from './reducer/inventory/inventory.reducer';
import { notificationInitialState } from './reducer/notification/notification.reducer';
import { loadingInitialState } from './reducer/loading/loading.reducer';
import { searchInitialState } from './reducer/search/search.reducer';
import { inventorySettingsEditingInitialState } from './reducer/inventory-settings-editing/inventory-settings-editing.reducer';

console.info('Fast Inventory App');

moment.locale('pl');

const defaultState: State = {
	loading: loadingInitialState,
	inventory: inventoryInitialState,
	inventorySettingsEditing: inventorySettingsEditingInitialState,
	notification: notificationInitialState,
	search: searchInitialState
};

const logger: Middleware = createLogger({
	level: 'info',
	collapsed: true
});

const router: Middleware = routerMiddleware(hashHistory);

const store: any = createStore(
	reducer,
	defaultState,
	applyMiddleware(router, logger, inventoryService())
);

const history: ReactRouterReduxHistory = syncHistoryWithStore(hashHistory, store);

ReactDom.render(
	<Provider store={store}>
		<Router history={history}>
			<Route path="/" component={AppComponent}>
				<IndexRoute component={ConnectedInventoryPage}/>
				<Route path="/print" component={ConnectedPrintPage} />
			</Route>
		</Router>
	</Provider>,
	document.getElementById('content')
);
