import * as React from 'react';
import { Dispatch } from 'redux';
import { connect, MapStateToProps, MapDispatchToPropsFunction } from 'react-redux';
import { ConnectedInventoryNotificationComponent } from '../components/inventory-notification/inventory-notification.component';
import { ConnectedInventoryHeaderComponent } from '../components/inventory-header/inventory-header.component';
import { ConnectedProductsListComponent } from '../components/products-list/products-list.component';
import { ConnectedInventoryFormComponent } from '../components/inventory-form/inventory-form.component';
import { ConnectedEditProductFormComponent } from '../components/edit-product-form/edit-product-form.component';
import { State } from '../state';
import { Product } from '../../domain';
import { createFetchInventoryAction } from '../actions/inventory.actions';

import './page.styl';

type Props = {
	loading: boolean,
	products: Product[],
	onLoad(): void
}

class InventoryPage extends React.Component<Props, {}> {

	componentWillMount(): void {
		if (!this.props.products.length) {
			this.props.onLoad();
		}
	}

	render(): JSX.Element {
		return (
			this.props.loading ? (
				<div className="loader"></div>
			) : (
				<div>
					<ConnectedInventoryNotificationComponent />
					<ConnectedInventoryHeaderComponent />
					<ConnectedProductsListComponent />

					<ConnectedInventoryFormComponent />
					<ConnectedEditProductFormComponent />
				</div>
			)
		);
	}

}

const mapStateToProps: MapStateToProps<{}, {}> = (state: State): Object => {
	return {
		loading: state.loading,
		products: state.inventory.products
	}
};

const mapDispatchToProps: MapDispatchToPropsFunction<{}, {}> = (dispatch: Dispatch<any>): Object => {
	return {
		onLoad: (): void => {
			dispatch(createFetchInventoryAction())
		}
	}
};

export const ConnectedInventoryPage: React.ComponentClass<{}> = connect(mapStateToProps, mapDispatchToProps)(InventoryPage);
