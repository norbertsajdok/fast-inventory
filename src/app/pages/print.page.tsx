import * as React from 'react';
import { Dispatch } from 'redux';
import { connect, MapStateToProps, MapDispatchToPropsFunction } from 'react-redux';
import { State } from '../state';
import { Product } from '../../domain';
import { ConnectedReportComponent } from '../components/report/report.component';
import { createFetchInventoryAction } from '../actions/inventory.actions';

import './page.styl';

type Props = {
	loading: boolean,
	products: Product[],
	onLoad(): void
}

class PrintPage extends React.Component<Props, {}> {

	componentWillMount(): void {
		if (!this.props.products.length) {
			this.props.onLoad();
		}
	}

	render(): JSX.Element {
		return (
			this.props.loading ? (
				<div className="loader"></div>
			) : (
				<div>
					<ConnectedReportComponent />
				</div>
			)
		);
	}

}

const mapStateToProps: MapStateToProps<{}, {}> = (state: State): Object => {
	return {
		loading: state.loading,
		products: state.inventory.products
	}
};

const mapDispatchToProps: MapDispatchToPropsFunction<{}, {}> = (dispatch: Dispatch<any>): Object => {
	return {
		onLoad: (): void => {
			dispatch(createFetchInventoryAction())
		}
	}
};

export const ConnectedPrintPage: React.ComponentClass<{}> = connect(mapStateToProps, mapDispatchToProps)(PrintPage);
