import { Reducer, combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { loadingReducer } from './loading/loading.reducer';
import { inventoryReducer } from './inventory/inventory.reducer';
import { notificationReducer } from './notification/notification.reducer';
import { searchReducer } from './search/search.reducer';
import { inventorySettingsEditingReducer } from './inventory-settings-editing/inventory-settings-editing.reducer';

export const reducer: Reducer<any> = combineReducers({
	loading: loadingReducer,
	inventory: inventoryReducer,
	inventorySettingsEditing: inventorySettingsEditingReducer,
	notification: notificationReducer,
	routing: routerReducer,
	search: searchReducer
});
