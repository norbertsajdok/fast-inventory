import { expect } from 'chai';
import { inventorySettingsEditingReducer, inventorySettingsEditingInitialState } from './inventory-settings-editing.reducer';
import { ACTIONS } from '../../actions/inventory.actions';

describe('Reducer: inventory settings editing', () => {
	it('should return the initial state', () => {
		const action: any = {
			type: ACTIONS.FETCH_INVENTORY
		};

		expect(
			inventorySettingsEditingReducer(inventorySettingsEditingInitialState, action)
		).to.deep.equal(inventorySettingsEditingInitialState);
	});

	it('should handle SELECT_INVENTORY_SETTINGS_TO_EDIT', () => {
		const action: any = {
			type: ACTIONS.SELECT_INVENTORY_SETTINGS_TO_EDIT
		};

		expect(
			inventorySettingsEditingReducer(inventorySettingsEditingInitialState, action)
		).to.deep.equal(true);
	});

	it('should handle SELECT_INVENTORY_SETTINGS_TO_EDIT', () => {
		const action: any = {
			type: ACTIONS.UNSELECT_INVENTORY_SETTINGS_TO_EDIT
		};

		expect(
			inventorySettingsEditingReducer(inventorySettingsEditingInitialState, action)
		).to.deep.equal(false);
	});
});
