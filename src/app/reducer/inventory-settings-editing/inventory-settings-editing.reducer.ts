import { Reducer } from 'redux';
import { ACTIONS } from '../../actions/inventory.actions';

export const inventorySettingsEditingInitialState: boolean = false;

export const inventorySettingsEditingReducer: Reducer<boolean> = (
	state: boolean = inventorySettingsEditingInitialState,
	action: any
): boolean => {
	switch (action.type) {
		case ACTIONS.SELECT_INVENTORY_SETTINGS_TO_EDIT:
			return true;
		case ACTIONS.UNSELECT_INVENTORY_SETTINGS_TO_EDIT:
			return false;
		default:
			return inventorySettingsEditingInitialState;
	}
};
