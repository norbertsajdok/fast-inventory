import { expect } from 'chai';
import * as update from 'immutability-helper';
import { inventoryReducer, inventoryInitialState } from './inventory.reducer';
import { ACTIONS } from '../../actions/inventory.actions';
import { Product, Inventory, InventorySettings } from '../../../domain';
import { response } from '../../fixtures/inventory';

describe('Reducer: inventory', () => {
	it('should return the initial state', () => {
		const action: any = {
			type: ACTIONS.FETCH_INVENTORY
		};

		expect(
			inventoryReducer(inventoryInitialState, action)
		).to.deep.equal(inventoryInitialState);
	});

	it('should handle FETCH_INVENTORY_SUCCESS', () => {
		const action: any = {
			type: ACTIONS.FETCH_INVENTORY_SUCCESS,
			inventory: new Inventory(response.inventory)
		};

		expect(
			inventoryReducer(inventoryInitialState, action)
		).to.deep.equal(action.inventory);
	});

	it('should handle ADD_PRODUCT_SUCCESS', () => {
		const currentState: Inventory = new Inventory(response.inventory);
		const action: any = {
			type: ACTIONS.ADD_PRODUCT_SUCCESS,
			product: new Product({
				id: '0',
				name: 'product name',
				unit: 'product unit',
				quantity: 3.75,
				net_price: {
					amount: 5.25
				}
			})
		};
		const expectedState: Inventory = {
			products: update(currentState.products, {
				$push: [action.product]
			}),
			settings: currentState.settings
		};

		expect(
			inventoryReducer(currentState, action)
		).to.deep.equal(expectedState);
	});

	it('should handle EDIT_PRODUCT_SUCCESS', () => {
		const currentState: Inventory = new Inventory(response.inventory);
		const action: any = {
			type: ACTIONS.EDIT_PRODUCT_SUCCESS,
			product: currentState.products[0].update({
				name: 'updated product name',
				unit: 'updated product unit',
				quantity: '2.25',
				net_price: '4.50'
			})
		};
		const expectedState: Inventory = {
			products: update(currentState.products, {
				[currentState.products.findIndex((product: Product) => product.id === action.product.id)]: { $set: action.product }
			}),
			settings: currentState.settings
		};

		expect(
			inventoryReducer(currentState, action)
		).to.deep.equal(expectedState);
	});

	it('should handle REMOVE_PRODUCT_SUCCESS', () => {
		const currentState: Inventory = new Inventory(response.inventory);
		const action: any = {
			type: ACTIONS.REMOVE_PRODUCT_SUCCESS,
			product: new Product(response.inventory.products[1])
		};
		const expectedState: Inventory = {
			products: update(currentState.products, {
				$splice: [[currentState.products.findIndex((product: Product) => product.id === action.product.id), 1]]
			}),
			settings: currentState.settings
		};

		expect(
			inventoryReducer(currentState, action)
		).to.deep.equal(expectedState);
	});

	it('should handle EDIT_INVENTORY_SETTINGS_SUCCESS', () => {
		const currentState: Inventory = new Inventory(response.inventory);
		const action: any = {
			type: ACTIONS.EDIT_INVENTORY_SETTINGS_SUCCESS,
			settings: new InventorySettings({
				companyName: 'Sklep spożywczy',
				companyAddress: 'ul. 3 Maja 30, Katowice',
				committee: 'Jan Kowalski, Andrzej Nowak',
				others: 'Anna Kowalska',
			})
		};
		const expectedState: Inventory = {
			products: currentState.products,
			settings: new InventorySettings({
				companyName: 'Sklep spożywczy',
				companyAddress: 'ul. 3 Maja 30, Katowice',
				committee: 'Jan Kowalski, Andrzej Nowak',
				others: 'Anna Kowalska',
			})
		};

		expect(
			inventoryReducer(currentState, action)
		).to.deep.equal(expectedState);
	});
});
