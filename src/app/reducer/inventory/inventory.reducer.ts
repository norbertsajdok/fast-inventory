import { Reducer } from 'redux';
import * as update from 'immutability-helper';
import { ACTIONS } from '../../actions/inventory.actions';
import { Product, Inventory } from '../../../domain';

export const inventoryInitialState: Inventory = {
	products: [],
	settings: {
		companyName: '',
		companyAddress: '',
		committee: '',
		others: '',
		startDate: '',
		endDate: ''
	}
};

export const inventoryReducer: Reducer<Inventory> = (
	state: Inventory = inventoryInitialState,
	action: any
): Inventory => {
	switch (action.type) {
		case ACTIONS.FETCH_INVENTORY_SUCCESS:
			return action.inventory;
		case ACTIONS.ADD_PRODUCT_SUCCESS:
			state.products = update(state.products, { $push: [action.product]});
			return state;
		case ACTIONS.EDIT_PRODUCT_SUCCESS:
			state.products = update(state.products, { [state.products.findIndex((product: Product) => product.id === action.product.id)]: { $set: action.product }});
			return state;
		case ACTIONS.REMOVE_PRODUCT_SUCCESS:
			state.products = update(state.products, { $splice: [[state.products.findIndex((product: Product) => product.id === action.product.id), 1]]});
			return state;
		case ACTIONS.EDIT_INVENTORY_SETTINGS_SUCCESS:
			return Object.assign({}, state, {
				settings: action.settings
			});
		default:
			return state;
	}
};
