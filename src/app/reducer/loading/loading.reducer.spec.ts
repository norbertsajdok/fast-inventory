import { expect } from 'chai';
import { loadingReducer, loadingInitialState } from './loading.reducer';
import { ACTIONS } from '../../actions/inventory.actions';
import { response } from '../../fixtures/inventory';
import { Inventory } from '../../../domain';

describe('Reducer: loading', () => {
	it('should return the initial state', () => {
		const action: any = {
			type: ACTIONS.FETCH_INVENTORY
		};

		expect(
			loadingReducer(loadingInitialState, action)
		).to.deep.equal(loadingInitialState);
	});

	it('should handle FETCH_INVENTORY_SUCCESS', () => {
		const action: any = {
			type: ACTIONS.FETCH_INVENTORY_SUCCESS,
			inventory: new Inventory(response.inventory)
		};

		expect(
			loadingReducer(loadingInitialState, action)
		).to.deep.equal(false);
	});
});
