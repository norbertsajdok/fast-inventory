import { Reducer } from 'redux';
import { ACTIONS } from '../../actions/inventory.actions';

export const loadingInitialState: boolean = true;

export const loadingReducer: Reducer<boolean> = (
	state: boolean = loadingInitialState,
	action: any
): boolean => {
	switch (action.type) {
		case ACTIONS.FETCH_INVENTORY_SUCCESS:
			return false;
		default:
			return state;
	}
};
