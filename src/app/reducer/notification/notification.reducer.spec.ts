import { expect } from 'chai';
import { notificationReducer, notificationInitialState } from './notification.reducer';
import { ACTIONS } from '../../actions/inventory.actions';
import { Product } from '../../../domain';
import { response } from '../../fixtures/inventory';

describe('Reducer: notification', () => {
	it('should return the initial state', () => {
		const action: any = {
			type: ACTIONS.FETCH_INVENTORY
		};

		expect(
			notificationReducer(notificationInitialState, action)
		).to.deep.equal({
			type: 'default',
			text: ''
		});
	});

	it('should handle ADD_PRODUCT_SUCCESS', () => {
		const action: any = {
			type: ACTIONS.ADD_PRODUCT_SUCCESS,
			product: new Product(response.inventory.products[0])
		};

		expect(
			notificationReducer(notificationInitialState, action)
		).to.deep.equal({
			type: 'success',
			text: 'Produkt \"Sprite\" został dodany.'
		});
	});

	it('should handle EDIT_PRODUCT_SUCCESS', () => {
		const action: any = {
			type: ACTIONS.EDIT_PRODUCT_SUCCESS,
			product: new Product(response.inventory.products[0])
		};

		expect(
			notificationReducer(notificationInitialState, action)
		).to.deep.equal({
			type: 'success',
			text: 'Produkt \"Sprite\" został zaaktualizowany.'
		});
	});

	it('should handle REMOVE_PRODUCT_SUCCESS', () => {
		const action: any = {
			type: ACTIONS.REMOVE_PRODUCT_SUCCESS,
			product: new Product(response.inventory.products[0])
		};

		expect(
			notificationReducer(notificationInitialState, action)
		).to.deep.equal({
			type: 'success',
			text: 'Produkt \"Sprite\" został usunięty.'
		});
	});

	it('should handle ADD_PRODUCT_ERROR', () => {
		const action: any = {
			type: ACTIONS.ADD_PRODUCT_ERROR
		};

		expect(
			notificationReducer(notificationInitialState, action)
		).to.deep.equal({
			type: 'error',
			text: 'Wystąpił błąd. Spróbuj ponownie.'
		});
	});

	it('should handle EDIT_PRODUCT_ERROR', () => {
		const action: any = {
			type: ACTIONS.EDIT_PRODUCT_ERROR
		};

		expect(
			notificationReducer(notificationInitialState, action)
		).to.deep.equal({
			type: 'error',
			text: 'Wystąpił błąd. Spróbuj ponownie.'
		});
	});

	it('should handle REMOVE_PRODUCT_ERROR', () => {
		const action: any = {
			type: ACTIONS.REMOVE_PRODUCT_ERROR
		};

		expect(
			notificationReducer(notificationInitialState, action)
		).to.deep.equal({
			type: 'error',
			text: 'Wystąpił błąd. Spróbuj ponownie.'
		});
	});
});
