import { Reducer } from 'redux';
import { ACTIONS } from '../../actions/inventory.actions';
import { Notification } from '../../state';

export const notificationInitialState: Notification = {
	type: 'default',
	text: ''
};

export const notificationReducer: Reducer<Notification> = (
	state: Notification = notificationInitialState,
	action: any
): Notification => {
	switch (action.type) {
		case ACTIONS.ADD_PRODUCT_SUCCESS:
			return {
				type: 'success',
				text: `Produkt "${action.product.name}" został dodany.`
			};
		case ACTIONS.EDIT_PRODUCT_SUCCESS:
			return {
				type: 'success',
				text: `Produkt "${action.product.name}" został zaaktualizowany.`
			};
		case ACTIONS.REMOVE_PRODUCT_SUCCESS:
			return {
				type: 'success',
				text: `Produkt "${action.product.name}" został usunięty.`
			};
		case ACTIONS.ADD_PRODUCT_ERROR:
		case ACTIONS.EDIT_PRODUCT_ERROR:
		case ACTIONS.REMOVE_PRODUCT_ERROR:
			return {
				type: 'error',
				text: 'Wystąpił błąd. Spróbuj ponownie.'
			};
		default:
			return notificationInitialState;
	}
};
