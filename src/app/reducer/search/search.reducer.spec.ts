import { expect } from 'chai';
import { searchReducer, searchInitialState } from './search.reducer';
import { ACTIONS } from '../../actions/inventory.actions';
import { response } from '../../fixtures/inventory';
import { Inventory, Product } from '../../../domain';
import { Search } from '../../state';

describe('Reducer: search', () => {
	it('should return the initial state', () => {
		const action: any = {
			type: ACTIONS.FETCH_INVENTORY
		};

		expect(
			searchReducer(searchInitialState, action)
		).to.deep.equal(searchInitialState);
	});

	it('should handle SEARCH_PRODUCT_SUCCESS', () => {
		const action: any = {
			type: ACTIONS.SEARCH_PRODUCT_SUCCESS,
			products: (new Inventory(response.inventory)).products
		};

		const expectedState: Search = {
			products: (new Inventory(response.inventory)).products,
			selectedProduct: null
		};

		expect(
			searchReducer(searchInitialState, action)
		).to.deep.equal(expectedState);
	});

	it('should handle SELECT_PRODUCT_TO_EDIT', () => {
		const action: any = {
			type: ACTIONS.SELECT_PRODUCT_TO_EDIT,
			product: new Product({
				id: '0',
				name: 'product name',
				unit: 'product unit',
				quantity: 3.75,
				net_price: {
					amount: 5.25
				}
			})
		};

		const expectedState: Search = {
			products: [],
			selectedProduct: new Product({
				id: '0',
				name: 'product name',
				unit: 'product unit',
				quantity: 3.75,
				net_price: {
					amount: 5.25
				}
			})
		};

		expect(
			searchReducer(searchInitialState, action)
		).to.deep.equal(expectedState);
	});
});
