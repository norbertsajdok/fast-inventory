import { Reducer } from 'redux';
import { ACTIONS } from '../../actions/inventory.actions';

import { Search } from '../../state';

export const searchInitialState: Search = {
	products: [],
	selectedProduct: null
};

export const searchReducer: Reducer<Search> = (
	state: Search = searchInitialState,
	action: any
): Search => {
	switch (action.type) {
		case ACTIONS.SEARCH_PRODUCT_SUCCESS:
			return Object.assign({}, state, {
				products: action.products
			});
		case ACTIONS.SELECT_PRODUCT_TO_EDIT:
			return Object.assign({}, state, {
				products: [],
				selectedProduct: action.product
			});
		default:
			return searchInitialState;
	}
};
