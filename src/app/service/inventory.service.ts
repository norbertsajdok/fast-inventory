import { Middleware } from 'redux';
import { ipcRenderer } from 'electron';
import {
	ACTIONS,
	createFetchInventorySuccessAction, createFetchInventoryErrorAction,
	createAddProductSuccessAction, createAddProductErrorAction,
	createEditProductSuccessAction, createEditProductErrorAction,
	createRemoveProductSuccessAction, createRemoveProductErrorAction,
	createSearchProductSuccessAction, createSearchProductErrorAction,
	createEditInventorySettingsSuccessAction, createEditInventorySettingsErrorAction
} from '../actions/inventory.actions';
import { FIInventory, Product } from '../../domain';

export type InventoryResponse = {
	inventory: FIInventory,
	message: string,
	status: string
}

export type SearchResponse = {
	products: Product[],
	message: string,
	status: string
}

type Service = () => Middleware;

export const inventoryService: Service = (): Middleware => {
	return (store: any) => (next: any) => (action: any) => {
		next(action);

		switch (action.type) {
			case ACTIONS.CREATE_INVENTORY:
				ipcRenderer.on('/api/inventory/create', (event: Electron.IpcRendererEvent, response: InventoryResponse): void => {
					if (response.status === 'error') {
						return store.dispatch(createFetchInventoryErrorAction(response.message));
					}

					ipcRenderer.removeAllListeners('/api/inventory/create');

					return store.dispatch(createFetchInventorySuccessAction(response));
				});

				ipcRenderer.send('/api/inventory/create');
				break;
			case ACTIONS.FETCH_INVENTORY:
				ipcRenderer.on('/api/inventory/fetch', (event: Electron.IpcRendererEvent, response: InventoryResponse): void => {
					if (response.status === 'error') {
						return store.dispatch(createFetchInventoryErrorAction(response.message));
					}

					ipcRenderer.removeAllListeners('/api/inventory/fetch');

					return store.dispatch(createFetchInventorySuccessAction(response));
				});

				ipcRenderer.send('/api/inventory/fetch');
				break;
			case ACTIONS.ADD_PRODUCT:
				ipcRenderer.on('/api/inventory/add-product', (event: Electron.IpcRendererEvent, response: InventoryResponse): void => {
					if (response.status === 'error') {
						return store.dispatch(createAddProductErrorAction(response.message));
					}

					ipcRenderer.removeAllListeners('/api/inventory/add-product');

					return store.dispatch(createAddProductSuccessAction(action.product));
				});

				ipcRenderer.send('/api/inventory/add-product', { product: action.product });
				break;
			case ACTIONS.EDIT_PRODUCT:
				ipcRenderer.on('/api/inventory/edit-product', (event: Electron.IpcRendererEvent, response: InventoryResponse): void => {
					if (response.status === 'error') {
						return store.dispatch(createEditProductErrorAction(response.message));
					}

					ipcRenderer.removeAllListeners('/api/inventory/edit-product');

					return store.dispatch(createEditProductSuccessAction(action.product));
				});

				ipcRenderer.send('/api/inventory/edit-product', { product: action.product });
				break;
			case ACTIONS.REMOVE_PRODUCT:
				ipcRenderer.on('/api/inventory/remove-product', (event: Electron.IpcRendererEvent, response: InventoryResponse): void => {
					if (response.status === 'error') {
						return store.dispatch(createRemoveProductErrorAction(response.message));
					}

					ipcRenderer.removeAllListeners('/api/inventory/remove-product');

					return store.dispatch(createRemoveProductSuccessAction(action.product));
				});

				ipcRenderer.send('/api/inventory/remove-product', { product: action.product });
				break;
			case ACTIONS.SEARCH_PRODUCT:
				ipcRenderer.on('/api/inventory/search-product', (event: Electron.IpcRendererEvent, response: SearchResponse): void => {
					if (response.status === 'error') {
						return store.dispatch(createSearchProductErrorAction(response.message));
					}

					ipcRenderer.removeAllListeners('/api/inventory/search-product');

					return store.dispatch(createSearchProductSuccessAction(response.products));
				});

				ipcRenderer.send('/api/inventory/search-product', { query: action.query });
				break;
			case ACTIONS.EDIT_INVENTORY_SETTINGS:
				ipcRenderer.on('/api/inventory/edit-settings', (event: Electron.IpcRendererEvent, response: InventoryResponse): void => {
					if (response.status === 'error') {
						return store.dispatch(createEditInventorySettingsErrorAction(response.message));
					}

					ipcRenderer.removeAllListeners('/api/inventory/edit-settings');

					return store.dispatch(createEditInventorySettingsSuccessAction(action));
				});

				ipcRenderer.send('/api/inventory/edit-settings', { settings: action.settings });
				break;
			default:
				break;
		}
	}
};
