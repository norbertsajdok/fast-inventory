import { Inventory, Product } from '../domain';

export type State = {
	loading: boolean;
	inventory: Inventory;
	inventorySettingsEditing: boolean;
	notification: Notification;
	search: Search;
}

export type Notification = {
	type: string;
	text: string;
}

export type Search = {
	products: Product[],
	selectedProduct: Nullable<Product>
}
