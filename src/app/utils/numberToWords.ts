const arrayOfUnits: string[] = ['', ' jeden', ' dwa', ' trzy', ' cztery', ' pięć', ' sześć', ' siedem', ' osiem', ' dziewięć'];
const arrayOfFirstRowOfDozens: string[] = ['', ' jedenaście', ' dwanaście', ' trzynaście', ' czternaście', ' piętnaście', ' szesnaście', ' siedemnaście', ' osiemnaście', ' dziewietnaście'];
const arrayOfDozens: string[] = ['', ' dziesięć', ' dwadzieścia', ' trzydzieści', ' czterdzieści', ' pięćdziesiąt', ' sześćdziesiąt', ' siedemdziesiąt', ' osiemdziesiąt', ' dziewięćdziesiąt'];
const arrayOfHundreds: string[] = ['', ' sto', ' dwieście', ' trzysta', ' czterysta', ' pięćset', ' sześćset', ' siedemset', ' osiemset', ' dziewięćset'];
const arrayOfGroups: string[][] = [
	['', '', ''],
	[' tysiąc', ' tysiące', ' tysięcy'],
	[' milion', ' miliony', ' milionów'],
	[' miliard', ' miliardy', ' miliardów'],
	[' bilion', ' biliony', ' bilionów'],
	[' biliard', ' biliardy', ' biliardów'],
	[' trylion', ' tryliony', ' tryliardów']
];

export const numberToWords: (number: number) => string = (number: number): string => {
	let numberToConvert: number = number;
	let numberInWords: string = '';

	if (!isNaN(numberToConvert)) {
		if (numberToConvert === 0) {
			numberInWords = 'zero';
		}

		let ordersOfMagnitude: number = 0;

		while (numberToConvert > 0) {
			const hundreds: number = Math.floor((numberToConvert % 1000) / 100);
			let dozens: number = Math.floor((numberToConvert % 100) / 10);
			let firstRowOfDozens: number = 0;
			let units: number = Math.floor(numberToConvert % 10);
			let form: number = 2;

			if (dozens === 1 && units > 0) {
				firstRowOfDozens = units;
				dozens = 0;
				units = 0;
			}

			if (units === 1 && hundreds + dozens + firstRowOfDozens === 0) {
				form = 0;
			}

			if (units === 2 || units === 3 || units === 4) {
				form = 1;
			}

			if (hundreds + dozens + firstRowOfDozens + units > 0) {
				if (ordersOfMagnitude === 1 && form === 0) {
					units = 0
				}

				numberInWords = arrayOfHundreds[hundreds] + arrayOfDozens[dozens] + arrayOfFirstRowOfDozens[firstRowOfDozens] + arrayOfUnits[units] + arrayOfGroups[ordersOfMagnitude][form] + numberInWords;
			}

			ordersOfMagnitude++;

			numberToConvert = Math.floor(numberToConvert / 1000);
		}

		return numberInWords.trim();
	}

	return numberInWords;
};
