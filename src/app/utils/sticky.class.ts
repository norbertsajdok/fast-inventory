const STICKY_CLASS: string = 'sticky';

type ElementOffset = {
	top: number,
	left: number
}

export class Sticky {
	private window: Window;
	private stickyElement: HTMLElement;
	private stickyElementOffset: ElementOffset;
	private stickyPlaceholder: Nullable<HTMLElement>;
	private cachedElementMargin: Nullable<string>;
	private resizeTimeout: any;

	constructor(element: Nullable<HTMLElement>, placeholder: Nullable<HTMLElement>) {
		if (!element) {
			console.warn('Element doesn\'t exists');
			return;
		}

		this.window = window;
		this.stickyElement = element as HTMLElement;
		this.stickyPlaceholder = placeholder;
		this.cachedElementMargin = this.stickyElement.style.marginTop;

		this.setStickyElementOffset();
		this.bindEvents();
	}

	private canBeSticky(): boolean {
		return this.window.innerHeight > this.stickyElement.offsetHeight;
	}

	private setStickyElementOffset(): void {
		this.stickyElementOffset = {
			top: this.stickyElement.getBoundingClientRect().top + document.body.scrollTop,
			left: this.stickyElement.getBoundingClientRect().left + document.body.scrollLeft
		};
	}

	private onScroll(): void {
		if (!this.canBeSticky()) {
			this.stickyElement.classList.remove(STICKY_CLASS);
			return;
		}

		const positionTop: number = this.window.scrollY;

		if (positionTop > this.stickyElementOffset.top) {
			if (!this.stickyElement.classList.contains(STICKY_CLASS)) {
				if (this.stickyPlaceholder) {
					Object.assign(this.stickyPlaceholder.style, {
						height: this.stickyElement.offsetHeight + 'px'
					});

					this.stickyPlaceholder.classList.toggle('hidden');
				}

				Object.assign(this.stickyElement.style, {
					width: this.stickyElement.offsetWidth + 'px',
					left: this.stickyElementOffset.left + 'px',
					marginTop: '0px'
				});
				this.stickyElement.classList.toggle(STICKY_CLASS);
			}
		}
		else if (this.stickyElement.classList.contains(STICKY_CLASS)) {
			Object.assign(this.stickyElement.style, {
				width: '100%',
				left: 'auto',
				marginTop: this.cachedElementMargin
			});
			this.stickyElement.classList.toggle(STICKY_CLASS);

			if (this.stickyPlaceholder) {
				this.stickyPlaceholder.classList.toggle('hidden');
			}
		}
	}

	private bindEvents(): void {
		this.window.addEventListener('scroll', (): void => this.onScroll());

		this.window.addEventListener('resize', (): void => {
			clearTimeout(this.resizeTimeout);
			this.resizeTimeout = setTimeout((): void => {
				this.setStickyElementOffset();
				this.onScroll();
			}, 50);
		});
	}
}
