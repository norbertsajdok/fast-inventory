export class UUID {
	static generate(): string {
		const chars: string = 'abcdefghijklmnopqrstuvwxyz0123456789';
		let uuid: string = '________-____-4___-____-____________';

		while (uuid.indexOf('_') !== -1) {
			uuid = uuid.replace('_', chars.charAt(Math.floor(Math.random() * chars.length)));
		}

		return uuid;
	}
}
