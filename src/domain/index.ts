export * from './inventory/inventory.class';
export * from './inventory-settings/settings.class';
export * from './price/price.class';
export * from './product/product.class';
