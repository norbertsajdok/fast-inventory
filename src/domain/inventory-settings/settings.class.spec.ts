import { expect } from 'chai';
import { InventorySettings } from './settings.class';

describe('Domain: inventory settings object', () => {
	let settings: InventorySettings;

	beforeEach(() => {
		settings = new InventorySettings({
			companyName: 'Sklep spożywczy',
			companyAddress: 'ul. 3 Maja 30, Katowice',
			committee: 'Jan Kowalski, Andrzej Nowak',
			others: 'Anna Kowalska',
			startDate: '2016-12-24T12:25',
			endDate: '2016-12-24T14:25'
		});
	});

	it('should have company name', () => {
		expect(settings.companyName).to.equal('Sklep spożywczy');
	});

	it('should have company address', () => {
		expect(settings.companyAddress).to.equal('ul. 3 Maja 30, Katowice');
	});

	it('should have info about committee', () => {
		expect(settings.committee).to.equal('Jan Kowalski, Andrzej Nowak');
	});

	it('should have info about other people', () => {
		expect(settings.others).to.equal('Anna Kowalska');
	});

	it('should have inventory start date', () => {
		expect(settings.startDate).to.equal('2016-12-24T12:25');
	});

	it('should have inventory end date', () => {
		expect(settings.endDate).to.equal('2016-12-24T14:25');
	});
});
