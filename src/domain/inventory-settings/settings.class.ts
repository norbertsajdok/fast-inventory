export type FIInventorySettings = {
	companyName: string;
	companyAddress: string;
	committee: string;
	others?: string;
	startDate?: string;
	endDate?: string;
}

export class InventorySettings {
	companyName: string;
	companyAddress: string;
	committee: string;
	others: Nullable<string>;
	startDate: Nullable<string>;
	endDate: Nullable<string>;

	constructor(inventorySettings: FIInventorySettings) {
		this.companyName = inventorySettings.companyName;
		this.companyAddress = inventorySettings.companyAddress;
		this.committee = inventorySettings.committee;
		this.others = inventorySettings.others || null;
		this.startDate = inventorySettings.startDate || null;
		this.endDate = inventorySettings.endDate || null;
	}

}
