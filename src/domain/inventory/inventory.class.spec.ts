import { expect } from 'chai';
import { Inventory } from './inventory.class';
import { InventorySettings } from '../inventory-settings/settings.class';

describe('Domain: inventory object', () => {
	let inventory: Inventory;

	beforeEach(() => {
		inventory = new Inventory({
			products: [{
				id: '0',
				name: 'product name',
				unit: 'product unit',
				quantity: 3.75,
				net_price: {
					amount: 5.25
				}
			}, {
				id: '1',
				name: 'product name',
				unit: 'product unit',
				quantity: 5.75,
				net_price: {
					amount: 4.25
				}
			}],
			settings: {
				companyName: 'Sklep spożywczy',
				companyAddress: 'ul. 3 Maja 30, Katowice',
				committee: 'Jan Kowalski, Andrzej Nowak',
				others: 'Anna Kowalska',
				startDate: '2016-12-24T12:25',
				endDate: '2016-12-24T14:25'
			}
		});
	});

	it('should have products collection', () => {
		expect(inventory.products.length).to.equal(2);
	});

	it('should have settings', () => {
		expect(inventory.settings).to.deep.equal(new InventorySettings({
			companyName: 'Sklep spożywczy',
			companyAddress: 'ul. 3 Maja 30, Katowice',
			committee: 'Jan Kowalski, Andrzej Nowak',
			others: 'Anna Kowalska',
			startDate: '2016-12-24T12:25',
			endDate: '2016-12-24T14:25'
		}));
	});

	describe('when inventory is empty', () => {
		beforeEach(() => {
			inventory = new Inventory({
				products: [],
				settings: null
			});
		});

		it('should have empty products collection', () => {
			expect(inventory.products.length).to.equal(0);
		});

		it('should have not settings', () => {
			expect(inventory.settings).to.undefined;
		});
	});
});
