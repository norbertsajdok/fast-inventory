import { Product, FIProduct } from '../product/product.class';
import { InventorySettings, FIInventorySettings } from '../inventory-settings/settings.class';

export type FIInventory = {
	products: FIProduct[];
	settings: Nullable<FIInventorySettings>;
}

export class Inventory {
	products: Product[] = [];
	settings: InventorySettings;

	constructor(inventory: FIInventory) {
		if (inventory.products.length) {
			this.products = inventory.products.map((product: FIProduct) => new Product(product));
		}

		if (inventory.settings) {
			this.settings = new InventorySettings(inventory.settings);
		}
	}
}
