import { expect } from 'chai';
import { FIPrice, Price } from './price.class';

describe('Domain: price object', () => {
	it('should create Price object', () => {
		const price: FIPrice = {
			amount: 3.54,
			currency: 'EUR'
		};

		const expectedPrice: Object = {
			amount: price.amount,
			currency: price.currency
		};

		expect(new Price(price)).to.deep.equal(expectedPrice);
	});

	it('should create Price object with default currency', () => {
		const price: FIPrice = {
			amount: 3.54
		};

		const expectedPrice: Object = {
			amount: price.amount,
			currency: 'PLN'
		};

		expect(new Price(price)).to.deep.equal(expectedPrice);
	});

	it('should return price as a string', () => {
		const price: FIPrice = {
			amount: 3.5,
			currency: 'EUR'
		};

		const expectedPrice: string = `${price.amount.toFixed(2)} ${price.currency}`;

		expect((new Price(price)).toString()).to.deep.equal(expectedPrice);
	});
});
