export type FIPrice = {
	amount: number,
	currency?: string
}

export class Price {
	amount: number;
	currency: string;

	constructor(price: FIPrice) {
		this.amount = Math.round(price.amount * 100) / 100;
		this.currency = price.currency || 'PLN';
	}

	toString(): string {
		return `${this.amount.toFixed(2)} ${this.currency}`;
	}
}
