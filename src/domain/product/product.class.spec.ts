import { expect } from 'chai';
import { Product } from './product.class';

describe('Domain: product object', () => {
	let product: Product;

	beforeEach(() => {
		product = new Product({
			id: 'uuid-placeholer',
			name: 'product name',
			unit: 'product unit',
			quantity: 3.75,
			net_price: {
				amount: 5.25
			}
		});
	});

	it('should have id', () => {
		expect(product.id).not.to.empty;
	});

	it('should have capitalized name', () => {
		expect(product.name).to.equal('Product name');
	});

	it('should have unit', () => {
		expect(product.unit).to.equal('product unit');
	});

	it('should have quantity', () => {
		expect(product.quantity).to.equal(3.75);
	});

	it('should have net price', () => {
		expect(product.net_price.toString()).to.equal('5.25 PLN');
	});

	it('should have net value', () => {
		expect(product.net_value.toString()).to.equal('19.69 PLN');
	});

	describe('when quantity is missing', () => {
		it('should not calculate net value', () => {
			product = new Product({
				id: 'uuid-placeholer',
				name: 'product name',
				unit: 'product unit',
				net_price: {
					amount: 5.25
				}
			});

			expect(product.net_value.toString()).to.equal('0.00 PLN');
		});
	});

	describe('when net price is missing', () => {
		it('should not calculate net value', () => {
			product = new Product({
				id: 'uuid-placeholer',
				name: 'product name',
				unit: 'product unit',
				quantity: 3.75
			});

			expect(product.net_value.toString()).to.equal('0.00 PLN');
		});
	});

	describe('when object has been updated', () => {
		beforeEach(() => {
			product = product.update({
				name: 'updated product name',
				unit: 'updated product unit',
				quantity: '2.25',
				net_price: '4.50'
			});
		});

		it('should have updated capitalized name', () => {
			expect(product.name).to.equal('Updated product name');
		});

		it('should have updated unit', () => {
			expect(product.unit).to.equal('updated product unit');
		});

		it('should have updated quantity', () => {
			expect(product.quantity).to.equal(2.25);
		});

		it('should have updated net price', () => {
			expect(product.net_price.toString()).to.equal('4.50 PLN');
		});

		it('should have updated net value', () => {
			expect(product.net_value.toString()).to.equal('10.13 PLN');
		});
	});
});
