import { Price, FIPrice } from '../price/price.class';

export type FIProduct = {
	id: string;
	name: string;
	unit: string;
	quantity?: number;
	net_price?: FIPrice;
	net_value?: FIPrice;
}

export class Product {
	id: string;
	name: string;
	unit: string;
	quantity: number;
	net_price: Price;
	net_value: Price;

	constructor(product: FIProduct) {
		this.id = product.id;
		this.name = this.formatName(product.name);
		this.unit = product.unit;

		if (typeof product.quantity !== undefined) {
			this.quantity = product.quantity || 0;
		}

		if (product.net_price) {
			this.net_price = new Price(product.net_price);
		}

		if (product.net_value) {
			this.net_value = new Price(product.net_value);
		}
		else {
			this.net_value = new Price({amount: this.getProductValue()})
		}
	}

	update(data: any): Product {
		return new Product({
			id: this.id,
			name: data.name || this.name,
			unit: data.unit || this.unit,
			quantity: +(data.quantity || this.quantity),
			net_price: {
				amount: +(data.net_price ? data.net_price.replace(this.net_price.currency, '') : this.net_price.amount)
			}
		})
	}

	private getProductValue(): number {
		if (this.quantity && this.net_price) {
			return this.quantity * this.net_price.amount;
		}

		return 0;
	}

	private formatName(name: string): string {
		return name.charAt(0).toUpperCase() + name.slice(1);
	}

}
