import { ipcMain } from 'electron';
import { InventoryModel } from './model';
import { FIInventory, Product } from '../../domain';

export class InventoryController {
	private currentInventoryKey: string;
	private model: InventoryModel;

	constructor(inventories: string[]) {
		this.currentInventoryKey = this.getCurrentInventoryKey(inventories) || this.generateInventoryKey();
		this.model = new InventoryModel(this.currentInventoryKey);

		this.routes();
	}

	static bootstrap(inventories: string[]): InventoryController {
		return new InventoryController(inventories);
	}

	private generateInventoryKey(): string {
		return `inventory-${+ new Date()}`;
	}

	private getCurrentInventoryKey(inventories: string[]): string {
		return inventories.reverse()[0];
	}

	private routes(): void {
		ipcMain.on('/api/inventory/create', (event: Electron.IpcMainEvent): void => {
			this.model = new InventoryModel(this.generateInventoryKey());

			this.model.getInventory()
				.then((response: FIInventory): void => {
					event.sender.send('/api/inventory/create', {
						status: 'done',
						inventory: response
					});
				})
				.catch((error: Error): void => {
					event.sender.send('/api/inventory/create', {
						status: 'error',
						message: error.message
					});
				});
		});

		ipcMain.on('/api/inventory/fetch', (event: Electron.IpcMainEvent): void => {
			this.model.getInventory()
				.then((response: FIInventory): void => {
					event.sender.send('/api/inventory/fetch', {
						status: 'done',
						inventory: response
					});
				})
				.catch((error: Error): void => {
					event.sender.send('/api/inventory/fetch', {
						status: 'error',
						message: error.message
					});
				});
		});

		ipcMain.on('/api/inventory/add-product', (event: Electron.IpcMainEvent, request: any): void => {
			this.model.addProduct(request)
				.then((): void => {
					event.sender.send('/api/inventory/add-product', {
						status: 'done',
						message: 'Product has been added to storage'
					});
				})
				.catch((error: Error): void => {
					event.sender.send('/api/inventory/add-product', {
						status: 'error',
						message: error.message
					});
				});
		});

		ipcMain.on('/api/inventory/edit-product', (event: Electron.IpcMainEvent, request: any): void => {
			this.model.editProduct(request)
				.then((): void => {
					event.sender.send('/api/inventory/edit-product', {
						status: 'done',
						message: 'Product has been updated in storage'
					});
				})
				.catch((error: Error): void => {
					event.sender.send('/api/inventory/edit-product', {
						status: 'error',
						message: error.message
					});
				});
		});

		ipcMain.on('/api/inventory/remove-product', (event: Electron.IpcMainEvent, request: any): void => {
			this.model.removeProduct(request)
				.then((): void => {
					event.sender.send('/api/inventory/remove-product', {
						status: 'done',
						message: 'Product has been removed from storage'
					});
				})
				.catch((error: Error): void => {
					event.sender.send('/api/inventory/remove-product', {
						status: 'error',
						message: error.message
					});
				});
		});

		ipcMain.on('/api/inventory/search-product', (event: Electron.IpcMainEvent, request: any): void => {
			this.model.searchProduct(request)
				.then((products: Product[]): void => {
					event.sender.send('/api/inventory/search-product', {
						status: 'done',
						products: products
					});
				})
				.catch((error: Error): void => {
					event.sender.send('/api/inventory/search-product', {
						status: 'error',
						message: error.message
					});
				});
		});

		ipcMain.on('/api/inventory/edit-settings', (event: Electron.IpcMainEvent, request: any): void => {
			this.model.editInventorySettings(request)
				.then((): void => {
					event.sender.send('/api/inventory/edit-settings', {
						status: 'done',
						message: 'Inventory settings has been updated in storage'
					});
				})
				.catch((error: Error): void => {
					event.sender.send('/api/inventory/edit-settings', {
						status: 'error',
						message: error.message
					});
				});
		});
	}
}
