import * as update from 'immutability-helper';
import { Storage } from './storage';
import { Product, Inventory, FIInventory } from '../../domain';

export class InventoryModel {
	private storage: Storage;
	private inventory: Inventory;

	constructor(storageName: string) {
		this.storage = new Storage(storageName);
	}

	getInventory(): Promise<any> {
		return new Promise((resolve: Function, reject: Function): void => {
			this.storage.get()
				.then((response: FIInventory): void => {
					if (!response.hasOwnProperty('products')) {
						response.products = [];
					}
					if (!response.hasOwnProperty('settings')) {
						response.settings = null;
					}

					this.inventory = new Inventory(response);
					resolve(response);
				})
				.catch((error: Error): void => reject(error));
		});
	}

	addProduct(request: any): Promise<any> {
		const product: Product = new Product({
			id: request.product.id,
			name: request.product.name,
			unit: request.product.unit,
			quantity: request.product.quantity,
			net_price: request.product.net_price,
			net_value: request.product.net_value
		});

		this.inventory.products = update(this.inventory.products, {
			$push: [product]
		});

		return new Promise((resolve: Function, reject: Function): void => {
			this.storage.set(this.inventory)
				.then((): void => resolve())
				.catch((error: Error): void => reject(error));
		});
	}

	editProduct(request: any): Promise<any> {
		this.inventory.products = update(this.inventory.products, {
			[this.inventory.products.findIndex((product: Product) => product.id === request.product.id)]: { $set: request.product }
		});

		return new Promise((resolve: Function, reject: Function): void => {
			this.storage.set(this.inventory)
				.then((): void => resolve())
				.catch((error: Error): void => reject(error));
		});
	}

	removeProduct(request: any): Promise<any> {
		this.inventory.products = update(this.inventory.products, {
			$splice: [[this.inventory.products.findIndex((product: Product) => product.id === request.product.id), 1]]
		});

		return new Promise((resolve: Function, reject: Function): void => {
			this.storage.set(this.inventory)
				.then((): void => resolve())
				.catch((error: Error): void => reject(error));
		});
	}

	searchProduct(request: any): Promise<any> {
		return new Promise((resolve: Function, reject: Function): void => {
			try {
				const results: Product[] = this.inventory.products.filter((product: Product): boolean => {
					return product.name.toLowerCase().indexOf(request.query.toLowerCase()) !== -1;
				});

				resolve(results);
			} catch (error) {
				reject(error);
			}
		});
	}

	editInventorySettings(request: any): Promise<any> {
		if (request.settings) {
			this.inventory.settings = update(this.inventory.settings, { $set: request.settings });
		}

		return new Promise((resolve: Function, reject: Function): void => {
			this.storage.set(this.inventory)
				.then((): void => resolve())
				.catch((error: Error): void => reject(error));
		});
	}
}
