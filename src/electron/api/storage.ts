import { writeFile, readFile, stat } from 'fs';
import * as rimraf from 'rimraf';
import { getFilePath } from '../utils/file';

export class Storage {
	private storageFile: string;

	constructor(storageName: string) {
		this.storageFile = getFilePath(storageName);
	}

	set(data: any): Promise<any> {
		return new Promise((resolve: Function, reject: Function): void => {
			writeFile(this.storageFile, JSON.stringify(data), (error: NodeJS.ErrnoException): void => {
				if (error) {
					reject(error);
				}
				else {
					resolve();
				}
			});
		});
	}

	has(): Promise<any> {
		return new Promise((resolve: Function, reject: Function): void => {
			stat(this.storageFile, (error: NodeJS.ErrnoException): void => {
				if (!error) {
					resolve(true);
				}
				else if (error.code === 'ENOENT') {
					resolve(false);
				}
				else {
					reject(error);
				}
			});
		});
	};

	get(): Promise<any> {
		return new Promise((resolve: Function, reject: Function): void => {
			readFile(this.storageFile, {
				encoding: 'utf8'
			}, (error: NodeJS.ErrnoException, response: string): void => {
				if (!error) {
					resolve(JSON.parse(response));
				}
				else if (error.code === 'ENOENT') {
					resolve({});
				}
				else {
					reject(error);
				}
			});
		});
	}

	remove(): Promise<any> {
		return new Promise((resolve: Function, reject: Function): void => {
			rimraf(this.storageFile, (error: NodeJS.ErrnoException): void => {
				if (error) {
					reject(error);
				}
				else {
					resolve();
				}
			});
		});
	}
}
