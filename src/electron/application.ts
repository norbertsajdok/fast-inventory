import { BrowserWindow, ipcMain } from 'electron';
import { env } from './utils/env';
import { saveToPdf } from './utils/print';
import { InventoryController } from './api/controller';
import { getAppDataFiles } from './utils/file';

type WindowSize = {
	width: number,
	height: number
}

const windowSize: WindowSize = {
	width: 1024,
	height: 768
};

export class Application {
	mainWindow: Nullable<Electron.BrowserWindow>;

	constructor() {
		this.mainWindow = new BrowserWindow(Object.assign({}, windowSize, {
			show: false
		}));

		if (env.isDevelopment()) {
			this.mainWindow.webContents.openDevTools();
		}

		this.mainWindow.loadURL(`file://${__dirname}/../static/index.html`);

		this.mainWindow.webContents.on('did-finish-load', () => {
			if (this.mainWindow) {
				this.mainWindow.show();
				this.mainWindow.focus();

				if (env.isDevelopment()) {
					this.mainWindow.maximize();
				}
			}
		});

		this.mainWindow.on('closed', () => {
			this.mainWindow = null;
		});

		getAppDataFiles((error: Nullable<Error>, inventories: string[]): void => {
			if (error) {
				throw new Error('Getting application data files failed');
			}

			InventoryController.bootstrap(inventories);
		});

		ipcMain.on('print-to-pdf', (event: Electron.IpcMainEvent): void => {
			saveToPdf(BrowserWindow.fromWebContents(event.sender), event);
		});
	}
}
