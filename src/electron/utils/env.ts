const PRODUCTION: string = 'production';
const DEVELOPMENT: string = 'development';

type Env = {
	isProduction(): boolean,
	isDevelopment(): boolean
}

export const env: Env = {
	isProduction: (): boolean => {
		return process.env.NODE_ENV === PRODUCTION;
	},

	isDevelopment: (): boolean => {
		return process.env.NODE_ENV === DEVELOPMENT;
	}
};
