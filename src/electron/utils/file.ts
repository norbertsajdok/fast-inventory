import { app } from 'electron';
import { basename, join } from 'path';
import { existsSync, mkdirSync, readdir, statSync } from 'fs';

type Callback = (error: Nullable<Error>, results: any) => void;

const FILE_EXTENSION: string = '.json';

export const getAppDataPath: () => string = (): string => {
	const homePath: string = `${app.getPath('home')}/Applications`;
	const appDir: string = '/FastInventory';

	if (!existsSync(homePath)) {
		mkdirSync(homePath);
	}

	if (!existsSync(`${homePath}${appDir}`)) {
		mkdirSync(`${homePath}${appDir}`);
	}

	return `${homePath}${appDir}`;
};

export const getFilePath: (name: string) => string = (name: string): string => {
	if (!name) {
		throw new Error('Missing file name');
	}

	if (name.trim().length === 0) {
		throw new Error('Invalid file name');
	}

	const appDataPath: string = getAppDataPath();
	const filename: string = basename(name, FILE_EXTENSION) + FILE_EXTENSION;

	return join(appDataPath, filename);
};

export const getAppDataFiles: (callback: Callback) => void = (callback: Callback): void => {
	const dirName: string = getAppDataPath();

	readdir(dirName, (error: NodeJS.ErrnoException, files: string[]): void => {
		if (error) {
			callback(error, null);
			return;
		}

		const sortedFiles: string[] = files.filter((file: string): boolean => {
			return file.indexOf('inventory') !== -1;
		}).sort((a: string, b: string): number => {
			return statSync(getFilePath(a)).mtime.getTime() - statSync(getFilePath(b)).mtime.getTime();
		}).map((file: string): string => {
			return file.replace(FILE_EXTENSION, '')
		});

		callback(null, sortedFiles);
	});
};
