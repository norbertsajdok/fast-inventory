import { writeFile } from 'fs';
import { dialog, shell } from 'electron';

const printToPdfSettings: Electron.PrintToPDFOptions = {
	landscape: true,
	marginsType: 0,
	printBackground: false,
	printSelectionOnly: false,
	pageSize: 'A4',
};

export const saveToPdf: Function = (
	windowToPrint: Electron.BrowserWindow,
	event: Electron.IpcMainEvent
): void => {
	if (!windowToPrint) {
		dialog.showErrorBox('Error', 'The printing window isn\'t created');
		return;
	}

	dialog.showSaveDialog(windowToPrint, {}, (filePath: string): void => {
		if (filePath) {
			windowToPrint.webContents.printToPDF(printToPdfSettings, (error: Error, data: Buffer): void => {
				if (error) {
					dialog.showErrorBox('Error', error.message);
					return;
				}

				writeFile(filePath, data, (error: NodeJS.ErrnoException): void => {
					if (error) {
						dialog.showErrorBox('Error', error.message);
						return;
					}

					event.sender.send('wrote-pdf', filePath);
					shell.openItem(filePath);
				});
			});
		}
	});
};
