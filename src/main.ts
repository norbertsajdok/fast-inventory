import { app } from 'electron';
import electronDebug = require('electron-debug');
import installer, { ExtensionReference, REACT_DEVELOPER_TOOLS, REDUX_DEVTOOLS } from 'electron-devtools-installer';
import { Application } from './electron/application';
import { env } from './electron/utils/env';

if (env.isDevelopment()) {
	electronDebug({
		enabled: true,
		showDevTools: 'bottom'
	});
}

const installExtensions: Function = async (): Promise<any> => {
	if (env.isDevelopment()) {
		const extensions: ExtensionReference[] = [
			REACT_DEVELOPER_TOOLS,
			REDUX_DEVTOOLS
		];

		for (const extension of extensions) {
			try {
				await installer(extension, false);
			} catch (error) {
				console.warn('Chrome extensions installation error', error);
			}
		}
	}
};

app.on('ready', async (): Promise<any> => {
	await installExtensions();

	new Application();
});

app.on('window-all-closed', app.quit);
