const webpack = require('webpack');

const TS_INCLUDE = /\.tsx?$/;
const TS_EXCLUDE = /node_modules/;

module.exports = {
	entry: ['es6-shim', './src/app/index.tsx'],
	output: {
		path: __dirname + '/dist/',
		filename: 'index.js'
	},
	devtool: 'source-map',
	target: 'electron-renderer',
	resolve: {
		extensions: ['', '.ts', '.tsx', '.js']
	},
	module: {
		preLoaders: [{
			test: TS_INCLUDE,
			exclude: TS_EXCLUDE,
			loader: 'tslint'
		}],
		loaders: [{
			test: TS_INCLUDE,
			exclude: TS_EXCLUDE,
			loader: 'ts-loader'
		}, {
			test: /\.styl$/,
			loader: 'style-loader!css-loader!stylus-loader'
		}]
	},
	plugins: [
		new webpack.optimize.OccurrenceOrderPlugin()
	]
};
