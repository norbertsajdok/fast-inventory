const webpack = require('webpack');
const path = require('path');

const TS_INCLUDE = /\.tsx?$/;
const TS_EXCLUDE = /node_modules/;

module.exports = {
	entry: {
		app: ['es6-shim', './src/main.ts'],
		index: ['es6-shim', './src/app/index.tsx']
	},
	output: {
		path: __dirname + '/dist/',
		filename: '[name].js',
		chunkFilename: '[name]-[id].js',
		publicPath: 'http://localhost:9000/dist/'
	},
	target: 'electron',
	resolve: {
		extensions: ['', '.ts', '.tsx', '.js', '.json']
	},
	module: {
		preLoaders: [{
			test: TS_INCLUDE,
			exclude: TS_EXCLUDE,
			loader: 'tslint'
		}],
		loaders: [{
			test: TS_INCLUDE,
			exclude: TS_EXCLUDE,
			loader: 'ts-loader'
		}, {
			test: /\.styl$/,
			loader: 'style-loader!css-loader!stylus-loader'
		}, {
			test: /\.json$/,
			loader: 'json-loader'
		}]
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoErrorsPlugin()
	]
};
