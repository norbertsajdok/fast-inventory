const webpack = require('webpack');

const TS_INCLUDE = /\.tsx?$/;
const TS_EXCLUDE = /node_modules/;

module.exports = {
	entry: ['es6-shim', './src/main.ts'],
	output: {
		path: __dirname + '/dist/',
		filename: 'main.js'
	},
	devtool: 'source-map',
	target: 'electron-main',
	resolve: {
		extensions: ['', '.ts', '.tsx', '.js', '.json']
	},
	module: {
		preLoaders: [{
			test: TS_INCLUDE,
			exclude: TS_EXCLUDE,
			loader: 'tslint'
		}],
		loaders: [{
			test: TS_INCLUDE,
			exclude: TS_EXCLUDE,
			loader: 'ts-loader'
		}, {
			test: /\.json$/,
			loader: 'json-loader'
		}]
	},
	node: {
		__dirname: false,
		__filename: false
	},
	plugins: []
};
